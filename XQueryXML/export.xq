(: XQuery main module :)
<pennterest>
<operator>Mockingjay</operator>
<web_url>www.isgovernmentshutdown.com</web_url>

{
    for $content in doc("export.xml")/database/CONTENTS/tuple,
        $contentID in $content/CONTENTID/text(),
        $source in $content/SOURCE/text(),
        $type in $content/TYPE/text(),
        $url in $content/URL/text()
        
        return <object> 
        <id>{$contentID}</id>
        <source>{$source}</source>
        <type>{$type}</type>
        <url>{$url}</url>  
        {
            for $tags in doc("export.xml")/database/TAGS/tuple
            where $source = $tags/SOURCE/text() 
            and $contentID = $tags/CONTENTID/text()
                        
            return <tag>{$tags/TAG/text()}</tag>                       
        }           
        </object>    
}

{
    
    for $user in doc("export.xml")/database/USERS/tuple,
        $loginEmail in $user/LOGINEMAIL/text(),
        $fName in $user/FIRSTNAME/text(),
        $lName in $user/LASTNAME/text(),
        $affiliation  in $user/AFFILIATION/text(),
        $interestS in $user/INTEREST/text()
                
        return <user>
            <login> {$loginEmail} </login>
            <email> {$loginEmail}</email>
            <givenname>{$fName}</givenname>
            <surname>{$lName}</surname>
            <affiliation>{$affiliation}</affiliation>
            <birthday>0000-00-00</birthday>
            {
                for $interest in tokenize($interestS, ',')
                return <interest>{normalize-space($interest)}</interest>
            }
            
            {
                for $friends in doc("export.xml")/database/FRIENDS/tuple
                where $friends/LOGINEMAIL/text() = $loginEmail
                
                return {
                    for $f in $friends/FRIENDLOGIN/text()
                    return <friend>{$f}</friend>
                }
            }
            
            {
            
                for $board in doc("export.xml")/database/BOARDS/tuple
                where $loginEmail = $board/LOGINEMAIL/text()
                return <board><name>{$board/BOARDNAME/text()}</name></board>
            
            }
            
            {
                for $pin in doc("export.xml")/database/PINS/tuple                
                where $loginEmail = $pin/LOGINEMAIL/text()

                return <pin>
                    <objectId>{$pin/CONTENTID/text()}</objectId>
                    <source>{$pin/SOURCE/text()}</source>
                    <board>{$pin/BOARDNAME/text()}</board>                                  
                </pin>
            }
            
            {
                for $rating in doc("export.xml")/database/RATINGS/tuple
                where $loginEmail = $rating/LOGINEMAIL/text()
                return <rating>
                    <objectId>{$rating/CONTENTID/text()}</objectId>
                    <source>{$rating/SOURCE/text()}</source>
                    <rating>{$rating/RATING/text()}</rating>
                </rating>
            }
                        
        </user>
}


</pennterest>