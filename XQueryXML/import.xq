(: XQuery main module :)
<pennterest>
<users>
{

for $uzr in doc("import.xml")/pennterest/user,
        $uzrLogin in $uzr/email/text(),
        $uzrGivenname in $uzr/givenname/text(),
        $uzrSurname in $uzr/surname/text(),
        $uzrAffiliation in $uzr/affiliation/text()
        
        return
        <tuple>
        
        <loginEmail>{$uzrLogin}</loginEmail>
        <FirstName>{$uzrGivenname}</FirstName>
        <LastName>{$uzrSurname}</LastName>
        <password>{$uzrLogin}</password>
        <affiliation>{$uzrAffiliation}</affiliation>
        <interest>{ 
            let $result := ''
            for $interest in $uzr/interest/concat(.,if(not(position()=last()))
                                                    then ','
                                                    else())
            
                let $result := string-join(({$result}, {$interest}), '')                         
            return {$result}        
            }
        </interest>
        </tuple>
}    
</users>

<friends>
{
for $uzr in doc("import.xml")/pennterest/user,
    $uzrLogin in $uzr/login/text()
    
    for $friend in $uzr/friend
        
        return
        <tuple>
        
        <loginEmail>{$uzrLogin}</loginEmail>
        <friendLogin>{$friend/text()}</friendLogin>
        </tuple>
}
</friends>

<boards>
{
    for $uzr in doc("import.xml")/pennterest/user,
        $uzrLogin in $uzr/login/text()
        
        for $boardName in $uzr/board/name/text()
            
            return
            <tuple>
            
            <loginEmail>{$uzrLogin}</loginEmail>
            <boardName>{$boardName}</boardName>
            </tuple>
}
</boards>

<contents>
{
for $content in doc("import.xml")/pennterest/object,
    $contentId in $content/id/text(),
    $source in $content/source/text(),
    $type in $content/type/text(),
    $url in $content/url/text() 
           
            return
            <tuple>
            
            <contentId>{$contentId}</contentId>
            <source>{$source}</source>
            <type>{$type}</type>
            <url>{$url}</url>
            </tuple>
}
</contents>

<tags>
{
    for $object in doc("import.xml")/pennterest/object,
        $contentId in $object/id/text(),
        $source in $object/source/text(),
        $tag in $object/tag    
    return <tuple>
        <contentId>{$contentId}</contentId>
        <source>{$source}</source>
        <tag>{$tag/text()}</tag>
        </tuple>

}
</tags>

<pins>
{               for $uzr in doc("import.xml")/pennterest/user,
                $uzrLogin in $uzr/login/text(),
                $pin in $uzr/pin,
                $pinObjId in $pin/objectId/text(),
                $pinSource in $pin/source/text(),
                $boardName in $pin/board/text()            
                return <tuple>
                
                 <contentID> {$pinObjId} </contentID>
                 <source> {$pinSource} </source>
                 <loginEmail>{$uzrLogin}</loginEmail>
                 <boardName> {$boardName} </boardName>
                 </tuple>

}
</pins>

<ratings>
{
            for $uzr in doc("import.xml")/pennterest/user,
                $uzrLogin in $uzr/login/text(),
                $rating in $uzr/rating,
                $rateObjId in $rating/objectId/text(),
                $rateSource in $rating/source/text(),
                $rateVal in $rating/rating/text()            
                return <tuple>
                
                 <contentId> {$rateObjId} </contentId>
                 <source> {$rateSource} </source>
                 <loginEmail>{$uzrLogin}</loginEmail>
                 <rating> {$rateVal} </rating>
                 </tuple>
}
</ratings>


 
</pennterest>