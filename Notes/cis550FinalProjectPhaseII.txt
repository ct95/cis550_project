CIS550 – Final Project Phase II 

Group Name: D
Group Member:  Eric Nida, Chen-Wei Tang, Xuan Liu, Chuya Guo


*******************
*Connection String*
*******************

Login as admin:

sqlplus 'adminuser@(DESCRIPTION=(ADDRESS=(PROTOCOL=TCP)(HOST=cis550project.cupvrcsnxzfa.us-west-2.rds.amazonaws.com)(PORT=1521))(CONNECT_DATA=(SID=PENNTR)))'

-------------------------

Login as guest:

sqlplus 'guest@(DESCRIPTION=(ADDRESS=(PROTOCOL=TCP)(HOST=cis550project.cupvrcsnxzfa.us-west-2.rds.amazonaws.com)(PORT=1521))(CONNECT_DATA=(SID=PENNTR)))'


Guest user name: guest
Guest password: guestuser

Schema Name: adminuser


--------------------------

select all tables:

SELECT table_name FROM user_tables;  



