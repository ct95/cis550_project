// Connect string to Oracle
var connectData = { "hostname": "cis550project.cupvrcsnxzfa.us-west-2.rds.amazonaws.com", "user": "adminuser", "password": "adminuser", "database": "PENNTR" };

var oracle =  require("oracle");

/////
// Query the oracle database, and call output_actors on the results
//
// res = HTTP result object sent back to the client
// tag = Tag to query for
function query_db(req, res, tag) {
  oracle.connect(connectData, function(err, connection) {
    if ( err ) {
    	console.log(err);
    } else {
	  	// selecting rows
	  	console.log(tag);
	
	    connection.execute("select T.contentid, T.source, C.URL, C.TYPE from tags T " + 
			"LEFT JOIN CONTENTS C ON T.CONTENTID = C.CONTENTID and T.SOURCE=C.SOURCE " +
			"where upper(T.tag)='" + tag.trim().toUpperCase() + "' ",
	  			   [], 
	  			   function(err, tagResults) {
	  	    if ( err ) {
	  	    	console.log(err);
	  	    } else {
	  	    	    if(tagResults.length > 0) {// found the user
	  	    	    	console.log(tagResults);
	  	    	    	output_tagResult(req, res, tag, tagResults);
                    }else {
                    	console.log('empty tagResults');
                    	invalid_tag(req, res, tag);
                    }
	  	    }
	
	  	}); // end connection.execute
    }
  }); // end oracle.connect
}

/////
// Given a set of query results, output a table
//
// res = HTTP result object sent back to the client
// tagResults = List object of query results
function output_tagResult(req, res, tag, tagResults) {	

	if(tagResults.length != 0){
	res.render('user/search',
		   { title: 'MockingJay Pennterest',
		     subTitle: "Match found",
		     user:req.session.user,
		     tag:tag,
		     urls: tagResults
		 }
	);
	}
	else{
		res.render('user/search', 
			{ title: 'MockingJay Pennterest',
			  subTitle: "No match found!",
			  user:req.session.user,
			  tag:tag,
			  urls: ''
			}
		);
	}

}

function invalid_tag(req, res, tag) {	
	res.render('user/search', 
		{ title: 'MockingJay Pennterest',
		  subTitle: "No match found!",
		  user:req.session.user,
		  tag:tag,
		  urls: ''
		}
	);
}


/////
// This is what's called by the main app 
exports.search_tag = function(req, res){
	console.log("req.body.tag = " + req.body.tag);
	query_db(req, res, req.body.tag);
};
