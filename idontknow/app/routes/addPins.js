// Connect string to Oracle
var connectData = { "hostname": "cis550project.cupvrcsnxzfa.us-west-2.rds.amazonaws.com", "user": "adminuser", "password": "adminuser", "database": "PENNTR" };

var oracle =  require("oracle");

var MongoClient = require('mongodb').MongoClient;
var Db = require('mongodb').Db,
Server = require('mongodb').Server,
ReplSetServers = require('mongodb').ReplSetServers,
ObjectID = require('mongodb').ObjectID,
Binary = require('mongodb').Binary,
GridStore = require('mongodb').GridStore,
Grid = require('mongodb').Grid,
Code = require('mongodb').Code,
BSON = require('mongodb').pure().BSON,
assert = require('assert');
var MongoDB = require('mongodb');
var fs = require('fs');
var request = require('request');
var http = require('http');

/////
// Query the oracle database, and call output_actors on the results
//
// res = HTTP result object sent back to the client
// tag = Tag to query for
function update_db(req, res, contentid, source, loginEmail, boardName) {
  oracle.connect(connectData, function(err, connection) {
    if ( err ) {
    	console.log(err);
    } else {

	  	connection.execute("SELECT * FROM CONTENTS C WHERE (SELECT COUNT(*) "
	  			 		   + "FROM PINS P WHERE C.SOURCE=P.SOURCE and " 
	  			 		   + "C.CONTENTID=P.CONTENTID and C.SOURCE='" + source
	  			 		   + "' and C.CONTENTID=" + contentid + ")=4",
                   [], 
                   function(err, results) {
                   console.log(results.length + " results found for cache");
                       if (err) {
                           console.log(err);
                       } else if (results.length > 0) {
                           add_to_cache(results);    
                       }

                   });

	  	// selecting rows
	    connection.execute("INSERT INTO Pins (contentID, source, loginEmail, boardName) " +
	    	"VALUES (" + contentid + ", '" +  source + "', '" +  loginEmail + "', '" + boardName + "') ",
	  			   [], 
	  			   function(err, pinResults) {
	  	    if ( err ) {
	  	    	console.log(err);
	  	    	output_pinResult(req, res, 'fail');
	  	    } else {
	  	    	output_pinResult(req, res, 'succeed');
	  	    }
	
	  	});
    }
  }); // end oracle.connect
}

/////
// Given a set of query results, output a table
//
// res = HTTP result object sent back to the client
// tagResults = List object of query results
function output_pinResult(req, res, pinResults) {	
	res.render('user/addPins', {
		results: pinResults
	});
}

function add_to_cache(results) {

	//console.log(JSON.stringify(results));
	var remoteMongo = 'mongodb://ericnida:mockingJay@ds061318.mongolab.com:61318/mockingjay';

	MongoClient.connect(remoteMongo, function(err, db) {
		var fileId = '';
		fileId = results[0].CONTENTID + results[0].SOURCE;
		var gridStore = new GridStore(db, fileId, 'w');
		var currentURL = results[0].URL;

		gridStore.open(function(err, gridStore) { 
			http.get(currentURL, function (response) {
				console.log("Attempting to cache " + currentURL);
				response.setEncoding('binary');
        
                var image2 = '';
 
                response.on('data', function(chunk) {
                    image2 += chunk;
                });

                response.on('end', function() {
             
                    image1 = new Buffer(image2,"binary");
                    gridStore.write(image1, function(err, gridStore) {
                        assert.equal(null, err);

                        // Close (Flushes the data to MongoDB)
                        gridStore.close(function(err, result) {
                            assert.equal(null, err);
                            console.log("URL " + currentURL + " cached");
                        });
                    });
                });

			});
		});
	});

}


/////
// This is what's called by the main app 
exports.addPins_updatDB = function(req, res){
	update_db(req, res, req.query.contentID, req.query.source, req.query.loginEmail, req.query.boardName);
};
