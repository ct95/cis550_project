// Connect string to Oracle
var connectData = { "hostname": "cis550project.cupvrcsnxzfa.us-west-2.rds.amazonaws.com", 
					"user": "adminuser", 
					"password": "adminuser", 
					"database": "PENNTR" };

var oracle =  require("oracle");


/////
// Query the oracle database, and call output_actors on the results
//
// res = HTTP result object sent back to the client
// name = Name to query for
function query_db(res,contentid, source) {
  oracle.connect(connectData, function(err, connection) {
    if ( err ) {
    	console.log(err);
    } else {
	  	// selecting rows	  	
	  	console.log('contentid = ' + JSON.stringify(contentid));
	  	console.log('source = ' + JSON.stringify(source));
	  	
	    connection.execute("SELECT tag FROM tags WHERE contentid='" + contentid + "' AND source='" + source + "'", 
	  			   [], 
	  			   function(err, results) {
	  	    if ( err ) {
	  	    	console.log(err);
	  	    } else {
  	    		  	connection.close(); // done with the connection
	  	    		//console.log('tag results = ' + JSON.stringify(results));
	  	    		for(index = 0; index < results.length; ++index) {
                    	console.log('tag results = ' + JSON.stringify(results[index]));
                    }	  	    		
	  	    		//console.log(typeof results);
	  	    		console.log('tag results size = ' + JSON.stringify(results).length);
	  	    		output_user(res,results);	  	    		
	  	    }
	
	  	}); // end connection.execute

    }
  }); // end oracle.connect
}


/////
// Given a set of query results, output a table
//
// res = HTTP result object sent back to the client
// name = Name to query for
// results = List object of query results
function output_user(res,results) {	

	if(results.length != 0){
		res.render('user/tag',
			   { tags: results
			}
		);
	}
	else{
		res.render('user/tag', 
			{ tags: ''	  
			}
		);
	}

}

/////
// This is what's called by the main app 
exports.do_work = function(req, res){
	query_db(res,req.query.contentid, req.query.source);
};
