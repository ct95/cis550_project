// Connect string to Oracle
var connectData = { "hostname": "cis550project.cupvrcsnxzfa.us-west-2.rds.amazonaws.com", 
					"user": "adminuser", 
					"password": "adminuser", 
					"database": "PENNTR" };

var oracle =  require("oracle");


/////
// Query the oracle database, and call output_actors on the results
//
// res = HTTP result object sent back to the client
// name = Name to query for
function query_db(req, res,loginEmail, contentid, source, rating) {
	console.log('parameter: loginEmail = ' + loginEmail);
	console.log('parameter: contentid = ' + contentid);
	console.log('parameter: source = ' + source);
	console.log('parameter: rating = ' + rating);
  oracle.connect(connectData, function(err, connection) {
    if ( err ) {
        console.log(err);
    } else {    	
        // selecting rows - user name
        connection.execute("select count(*) as count from Ratings where loginEmail='" + loginEmail 
        	+ "' and contentID='" + contentid + "' and source='" + source + "'",
        	[], 
            function(err, results) {
                if ( err ) {
                    console.log("ERROR from checking count from Rating: " + err);
                } else {
                    console.log(results[0].COUNT);

                    if(results[0].COUNT > 0) { // rating for this content already exists                            	
                    	connection.execute("UPDATE Ratings SET rating=" + rating + " WHERE loginEmail='" + loginEmail
                    	 					+ "' and source='" + source + "' and contentID='" + contentid + "'",
                    		[], 
                    		function(err, update_results){
                    			if(err){
                    				console.log("ERROR from updating Rating: " + err);
                    				addRating_results(req, res, '-1');
                    			}
                    			else{
                    				console.log(update_results.updateCount);                    				
                    				connection.close();
									addRating_results(req,res,update_results.updateCount);
                    			}
                       	}); // end of coonection execute for update ratings
                    }

                    else{ // create new ratings
                    	console.log("NEW RATING!!");
                    	connection.execute("INSERT INTO Ratings (contentID, source, loginEmail, rating) VALUES ('" + contentid 
                    						+ "', '" + source + "', '" + loginEmail + "', '" + rating + "')", 
                    		[], 
                    		function(err, insert_results){
                    			if(err){
                    				console.log("ERROR from inserting Rating: " + err);
                    				addRating_results(req, res, '-1');
                    			}
                    			else{
                    				console.log(insert_results.updateCount);                    				
                    				connection.close();
                    				addRating_results(req, res, insert_results.updateCount);
                    			}                    			
                    	});// end of connection for inserting ratings                            	
                    }
                }
            }); // end connection.execute
    }
  }); 
}


/////
// Given a set of query results, output a table
//
// res = HTTP result object sent back to the client
// name = Name to query for
// results = List object of query results
function addRating_results(req, res, results) {	
	res.render('user/addRating', 
		{ ratingResult: results}
	);

}

/////
// This is what's called by the main app 
exports.do_work = function(req, res){
	query_db(req, res, req.query.loginEmail, req.query.contentid, req.query.source, req.query.rating);
};
