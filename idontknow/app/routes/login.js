// Connect string to Oracle
var connectData = { "hostname": "cis550project.cupvrcsnxzfa.us-west-2.rds.amazonaws.com", "user": "adminuser", "password": "adminuser", "database": "PENNTR" };

var oracle =  require("oracle");

/////
// Query the oracle database, and call output_actors on the results
//
// res = HTTP result object sent back to the client
// tag = Tag to query for
function query_db(req, res,userLogin,password) {
  oracle.connect(connectData, function(err, connection) {
    if ( err ) {
        console.log('db connection error' + err);
    } else {
        // selecting rows - user name
        console.log('db connection safe');
        //console.log('userLogin = ' + userLogin);
        //console.log('password = ' + password);
        connection.execute("SELECT * FROM users WHERE loginEmail='" + userLogin + 
                "' AND password='" + password+ "' ", 
                   [], 
                   function(err, results) {
                        if ( err ) {
                            console.log(err);
                        } else {              
                        	
                            if(results.length > 0) {
                                var splitInterest = (JSON.stringify(results[0].INTEREST)).split(",");
                                var interestString = splitInterest.map(function(s) { return "'" + s.trim().toUpperCase() + "'"; }).join(", ");
                            
                                req.session.user = results[0];

                                connection.execute("SELECT BOARDNAME FROM BOARDS WHERE loginEmail='" + userLogin + "'",
                                    [],
                                    function(err, boardResults){
                                        if(err){
                                            console.log("Query Boards Error");
                                        }
                                        else{
                                            console.log(boardResults);
                                            connection.execute("SELECT * FROM CONTENTS C, (SELECT T1.CONTENTID, T1.SOURCE FROM ( " +
                                                            "SELECT K.CONTENTID,K.SOURCE, SUM(SCORE) FROM( SELECT R.CONTENTID, R.SOURCE, " +
                                                            "AVG(R.RATING) AS SCORE FROM RATINGS R GROUP BY R.CONTENTID, R.SOURCE " +
                                                            "UNION ALL SELECT R1.CONTENTID, R1.SOURCE, AVG(R1.RATING) " +
                                                            "FROM RATINGS R1, FRIENDS F WHERE F.LOGINEMAIL='" + userLogin + "' " + 
                                                            "AND   R1.LOGINEMAIL = F.FRIENDLOGIN GROUP BY R1.CONTENTID, R1.SOURCE " + 
                                                            "UNION ALL SELECT T.CONTENTID, T.SOURCE, 1.5*COUNT(*) AS SCORE FROM TAGS T " +
                                                            "WHERE UPPER(T.TAG) in " +  "("+ interestString + ")" +  "GROUP BY T.CONTENTID, T.SOURCE " +
                                                            ") K GROUP BY K.CONTENTID,K.SOURCE ORDER BY SUM(SCORE) DESC) T1 MINUS " +
                                                            "SELECT P.CONTENTID,P.SOURCE from PINS P WHERE P.LOGINEMAIL = '" + userLogin + "' " +
                                                            ") F WHERE rownum <=10 AND C.CONTENTID=F.CONTENTID AND C.SOURCE=F.SOURCE ",
                                                [], 
                                                function(err, recommendationResults) {
                                                    if ( err ) {
                                                        console.log('error from query: ' + err);
                                                    } 
                                                    else {
                                                        if(recommendationResults.length > 0) {                                              
                                                            output_recommendation(req, res, results, boardResults, recommendationResults);
                                                        }
                                                        connection.close(); // done with the connection
                                                    }
                                            }); // end of execute for recommendations             
                                        }
                                }); // end of execute for boards

                            }
                            else invalid_user(res);
                        }
                
                    }); // end connection.execute

    }
  }); // end oracle.connect
}

/////
// Given a set of query results, output a table
//
// res = HTTP result object sent back to the client
// tagResults = List object of query results
function output_recommendation(req, res, results, boardResults, recommendationResults) {	

	if(recommendationResults.length != 0){

    	res.render('user/home',
    		   { title: 'MockingJay Pennterest',
    		     subTitle: "Match found",
    		     user: results[0],
                 loginEmail: results[0].LOGINEMAIL,
                 boards: boardResults,
    		     recommendations: recommendationResults
    		 }
    	);
	}
	else{
		res.render('user/invalidInput', 
			{ title: 'MockingJay Pennterest'
			}
		);
	}

}

function invalid_user(res) {	
	res.render('user/invalidInput', 
		{ title: 'MockingJay Pennterest'
		}
	);
}


/////
// This is what's called by the main app 
exports.recommendation = function(req, res){
	console.log("req.body.userlogin = " + req.body.username);
	query_db(req, res, req.body.username, req.body.password);
};

exports.recommendation_loggedIN = function(req, res){
    console.log("req.session.user.loginEmail = " + JSON.stringify(req.session.user.LOGINEMAIL));
    query_db(req, res, req.session.user.LOGINEMAIL, req.session.user.PASSWORD);
};



                                                            // old query
                                                            // "SELECT * FROM CONTENTS C WHERE C.CONTENTID IN ( SELECT T1.CONTENTID " +   
                                                            // "FROM ( SELECT T.CONTENTID, SUM(SCORE) FROM(SELECT R.CONTENTID, AVG(R.RATING) AS SCORE " +
                                                            // "FROM RATINGS R GROUP BY R.CONTENTID UNION ALL SELECT R.CONTENTID, AVG(R.RATING) " +
                                                            // "FROM RATINGS R, FRIENDS F WHERE F.LOGINEMAIL ='" + userLogin + "' " + 
                                                            // " AND   R.LOGINEMAIL = F.FRIENDLOGIN GROUP BY R.CONTENTID UNION ALL " +
                                                            // "SELECT T.CONTENTID, 15*COUNT(*) AS SCORE FROM TAGS T " + 
                                                            // "WHERE UPPER(T.TAG) in " +  "("+ interestString + ")" +
                                                            // " GROUP BY T.CONTENTID ) T GROUP BY T.CONTENTID ORDER BY SUM(SCORE) DESC) T1 " + 
                                                            // " MINUS  SELECT P.CONTENTID from PINS P WHERE P.LOGINEMAIL = '" + userLogin + "' " +
                                                            // ")AND rownum <=10", //('NASA', 'panda')