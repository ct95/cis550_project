/*
compabale of handling simple JSON-style objects without methods and DOM nodes inside
order of the fields inside the object matters!
*/
Array.prototype.contains = function(v) {

    for(var i = 0; i < this.length; i++) {

        if(JSON.stringify(this[i]) === JSON.stringify(v)) return true;
    }
    return false;
};
Array.prototype.unique = function() {
    var arr = [];
    for(var i = 0; i < this.length; i++) {
        if(!arr.contains(this[i])) {
            arr.push(this[i]);
        }
    }
    return arr; 
}

/* How to use these two above: 
var duplicates = [1,3,4,2,1,2,3,8];
var uniques = duplicates.unique(); // result = [1,3,4,2,8]
*/



// Connect string to Oracle
var connectData = { "hostname": "cis550project.cupvrcsnxzfa.us-west-2.rds.amazonaws.com", "user": "adminuser", "password": "adminuser", "database": "PENNTR" };

var oracle =  require("oracle");

/////
// Query the oracle database, and call output_actors on the results
//
// res = HTTP result object sent back to the client
// name = Name to query for
function query_db(req, res, loginEmail, password) {
  oracle.connect(connectData, function(err, connection) {
    if ( err ) {
    	console.log(err);
    } 
    else {  	
		connection.execute("SELECT B.BOARDNAME, C.CONTENTID, C.SOURCE, C.TYPE, C.URL, T.TAG" + 
						   " FROM BOARDS B LEFT JOIN PINS P ON B.BOARDNAME = P.BOARDNAME and B.LOGINEMAIL = P.LOGINEMAIL" + 
						   " LEFT JOIN CONTENTS C ON  P.CONTENTID = C.CONTENTID and P.SOURCE = C.SOURCE" + 
						   " LEFT JOIN TAGS T ON C.CONTENTID = T.CONTENTID AND C.SOURCE  = T.SOURCE WHERE B.LOGINEMAIL ='" + loginEmail + "'", 
			[], 
			function(err, results){

				if ( err ) {
	  	    	console.log(err);
	  	    	} 

	  	    	else {
  	    		  	connection.close(); // done with the connection
	  	    		var cleanBds = getBoardNames(results);
	  	    		var cleanContents = getContents(results, cleanBds[0]); // sample element: {CONTENTID: 403, SOURCE: 'mockingJay'}

	  	    		var finalResult = new Array();
	  	    		// iterate through the boards
	  	    		for(var i = 0; i < cleanBds.length; i++){
	  	    			currentBoardName = cleanBds[i];

	  	    			var contents = new Array();
	  	    			var cleanContents = getContents(results, cleanBds[i]);

	  	    			for(var j = 0; j < cleanContents.length; j++){
	  	    				currentContent = cleanContents[j];
	  	    				var currentTags = getTags(results, currentContent);
	  	    				fullContent = new Object();
	  	    				fullContent.CONTENTID = currentContent.CONTENTID;
	  	    				fullContent.SOURCE = currentContent.SOURCE;
	  	    				fullContent.TYPE = currentContent.TYPE;
	  	    				fullContent.URL = currentContent.URL;
	  	    				fullContent.TAGS = currentTags;
	  	    				contents.push(fullContent);
	  	    			}

	  	    			var board = new Object();
	  	    			board.BOARDNAME = currentBoardName;
	  	    			board.CONTENTS = contents;	  	    			

	  	    			finalResult.push(board);
	  	    		}

	  	    		output_user(req, res, finalResult);
	  	    	}
			}
		); // end of connection.execute	  	
    }
  }); // end oracle.connect
}


/////
// Given a set of query results, output a table
//
// res = HTTP result object sent back to the client
// name = Name to query for
// results = List object of query results
function output_user(req, res, results) {	
	if(results.length != 0){
	res.render('user/actor',
		   { title:'MockingJay Pennterest',
		   	 user:req.session.user,
		     results: results,
		     loginEmail: req.session.user.LOGINEMAIL		     
		 }
	);
	}
	else{
		res.render('user/actor', 
			{ title: "No User Found",
			  results: '',
			  contents: ''
			}
		);
	}

}


function invalid_login(res) {	
	res.render('user/actor', 
		{ title: "No User Found",
		  results: '',
		  boards: '',
		  contents: ''
		}
	);
}


/*
return a string array containing all the board names
return an empty array if none found
*/
function getBoardNames(results){
	var bdNames = new Array();	
	// safety
	if(results !== undefined){
		// build the raw board names
		for(var i = 0; i < results.length; i++){
			bdNames[i] = results[i].BOARDNAME;
		}
		// clean up the board names
		bdNames = bdNames.unique();
	}
	return bdNames; 
}

/*
return an object array containing all the contents associated with the boardname
return an empty array if none found
*/
function getContents(results, boardname){
	var contents = new Array();

	// safety
	if(results !== undefined){
		// build the raw contents
		for(var i = 0; i < results.length; i++){

			if(results[i].BOARDNAME == boardname){
				content = new Object();
				content.CONTENTID = results[i].CONTENTID;
				content.SOURCE = results[i].SOURCE;
				content.TYPE = results[i].TYPE;
				content.URL = results[i].URL;
				contents.push(content);	
			}
			
		}
		// clean up contents;
		contents = contents.unique();
	}
	return contents; 
}

/*
given a list of results and a given content object, 
returns an object array of he tags associated with this content
return an empty array if none found
**/
function getTags(results, content){
	var tags = new Array();

	// safety
	if(results !== undefined){
		// build the raw tags
		for(var i = 0; i < results.length; i++){			
			if(results[i].CONTENTID == content.CONTENTID && results[i].SOURCE == content.SOURCE){
				tags.push(results[i].TAG);
			}
		}
		// clean up the tags; though not really necessary;
		tags = tags.unique();
	}
	return tags; 
}


/////
// This is what's called by the main app 
exports.do_work = function(req, res){
	query_db(res,req.query.name);
};

exports.log_in = function(req, res){
	query_db(req, res, req.body.LoginEmail, req.body.password);
};

exports.logIn = function(req, res){
	query_db(req, res,req.session.user.LOGINEMAIL, req.session.user.PASSWORD);
};