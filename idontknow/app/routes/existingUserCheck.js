// Connect string to Oracle
var connectData = { "hostname": "cis550project.cupvrcsnxzfa.us-west-2.rds.amazonaws.com", 
					"user": "adminuser", 
					"password": "adminuser", 
					"database": "PENNTR" };

var oracle =  require("oracle");


/////
// Query the oracle database, and call output_actors on the results
//
// res = HTTP result object sent back to the client
// name = Name to query for
function query_db(req, res,loginEmail, password, firstName, lastName, affiliation, interest) {
	console.log('loginEmail = ' + loginEmail);
  oracle.connect(connectData, function(err, connection) {
    if ( err ) {
        console.log(err);
    } else {
        // selecting rows - user name
        connection.execute("SELECT loginEmail FROM users WHERE loginEmail='" + loginEmail + "' ", 
                   [], 
                   function(err, results) {
                        if ( err ) {
                            console.log(err);
                        } else {
                            console.log('results size = ' + results.length);
                            console.log('results = ' + JSON.stringify(results));
                            if(results.length > 0) { // loginEmail already exists
                            	existingUserCheck_result(req, res, 'true');
                            }else{ // valid new user
                            	console.log('userLogin: ' + req.query.loginEmail);
							    console.log('password: ' + req.query.password);
							    console.log('firstName: ' + req.query.firstName);
							    console.log('lastName: ' + req.query.lastName);
							    console.log('affiliation: ' + req.query.affiliation);
							    console.log('interest: ' + req.query.interest);
							    //console.log("========================");
							    connection.execute("INSERT INTO Users (loginEmail, firstName, lastName, password, affiliation, interest) " +
							    	"VALUES ('" + loginEmail + "', '" + firstName + "', '" + lastName + "', '" + 
							    		password + "', '" + affiliation + "', '" + interest + "') ",
							    	[],
							    	function(err, results_insertion) {
							    		if(err) {
							    			console.log('err!: ' + err);
							    		}else {
							    			console.log('results_insertion: ' + JSON.stringify(results_insertion));
							    			connection.execute("INSERT INTO Boards (loginEmail, boardName) " + 
							    				"VALUES ('" + loginEmail + "', '" + firstName + "Board1') ",
							    				[],
							    				function(err, defaultBoard_insertion) {
							    					if(err) {
							    						console.log('err at defaultBoard_insertion' + err);
							    					}else {
							    						console.log('defaultBoard_insertion: ' + JSON.stringify(defaultBoard_insertion));
							    						connection.close(); // done with the connection
							    						existingUserCheck_result(req, res, 'false');
							    					}
							    				})							    			
							    		}
							    	}
							 	);	
                            }
                        }
                    }); // end connection.execute
    }
  }); 
}


/////
// Given a set of query results, output a table
//
// res = HTTP result object sent back to the client
// name = Name to query for
// results = List object of query results
function existingUserCheck_result(req, res, results) {	

	if(results.length > 0){
		res.render('user/existingUserCheck',
			   { existing: results
			}
		);
	}
	else{
		res.render('user/existingUserCheck', 
			{ existing: results	  
			}
		);
	}

}

/////
// This is what's called by the main app 
exports.do_work = function(req, res){
	query_db(req, res, req.query.loginEmail, req.query.password, req.query.firstName, req.query.lastName, req.query.affiliation, req.query.interest);
};
