// Connect string to Oracle
var connectData = { "hostname": "cis550project.cupvrcsnxzfa.us-west-2.rds.amazonaws.com", "user": "adminuser", "password": "adminuser", "database": "PENNTR" };

var oracle =  require("oracle");

/////
// Query the oracle database, and call output_actors on the results
//
// res = HTTP result object sent back to the client
// name = Name to query for
function query_db(req, res, loginEmail, password) {
  oracle.connect(connectData, function(err, connection) {
    if ( err ) {
    	console.log(err);
    } else {
	  	// selecting rows	  	
	  	console.log('loginEmail = ' + loginEmail);
	  	console.log('password = ' + password);
	    connection.execute("SELECT * FROM users WHERE loginEmail='" + loginEmail + 
	  			"' AND password='" + password +"'", 
	  			   [], 
	  			   function(err, results) {
	  	    if ( err ) {
	  	    	console.log(err);
	  	    } else {
	  	    	    if(results.length > 0) {// found the user
	  	    	    	console.log(results);
				req.session.user = results[0];
				console.log('req.session.user.FIRSTNAME = ' + req.session.user.FIRSTNAME); 
                                 connection.execute("SELECT b.boardname FROM boards b WHERE b.loginEmail='" + loginEmail + "' ", 
                                        [], 
                                        function(err, boardresults) {
                                             if ( err ) {
                                                 console.log(err);
                                             } else {                                                 
                                                 if(boardresults.length > 0) { // found boards
                                             		console.log("boardsresults:")
                                             		console.log(boardresults);                                                 		
                                             		console.log("   ");

                                             		var boardSet = "'"+boardresults[0].BOARDNAME+"'";

                                             		for(var i = 1; i< boardresults.length; i++){
                                             			boardSet = boardSet + ",'" + boardresults[i].BOARDNAME + "'";
                                             		}
                                             		console.log("BoardSet:" + boardSet);

                                             		board0 = boardresults[1];
                                             		
                                        			connection.execute("select b.boardname, c.url, c.type, c.contentid, c.source from contents c, pins p, boards b where b.BOARDNAME IN(" + boardSet
                                        				+ ") and b.loginemail='" + loginEmail
                                        				+"' and b.loginemail=p.loginemail and b.boardname=p.boardname and p.source=c.source and p.contentid=c.contentid	", 
                                        				[], function(err, contentResults){
                                        					console.log("SQL results: ");
                                        					console.log(contentResults);
                                        					console.log("    ");

														    var organizedResults = new Array();
														    

														    if(boardresults != undefined && contentResults != undefined){
																for (i=0; i<boardresults.length;i++){
																	urlArray = new Array();
																	typeArray = new Array();
																	contentidArray = new Array();
																	sourceArray = new Array();
																	organizedResults[i] = {BOARDNAME: boardresults[i].BOARDNAME, URL:urlArray, TYPE: typeArray, CONTENTID: contentidArray, SOURCE: sourceArray};	
																}

																//console.log(organizedResults[0].BOARDNAME + " <- organizedResults[0].boardname");

																for(i = 0; i < boardresults.length; i++){
																	console.log("i=" +i);
																	for(j=0;j<contentResults.length; j++){                                        							
																		if(organizedResults[i].BOARDNAME == contentResults[j].BOARDNAME){
																			console.log("FOUND!");
																		 	numUrls = organizedResults[i].URL.length;
																			organizedResults[i].URL[numUrls] = contentResults[j].URL;
																			organizedResults[i].TYPE[numUrls] = contentResults[j].TYPE;
																			organizedResults[i].CONTENTID[numUrls] = contentResults[j].CONTENTID;
																			organizedResults[i].SOURCE[numUrls] = contentResults[j].SOURCE;
																		}
																		
																	}
																}
														    }
														    
                                        					console.log("organizedResults ")     
                                        					console.log(organizedResults);                                  					

                                        				output_user(req, res,loginEmail, results, organizedResults);
                                        			});

                                        			connection.close(); // done with the connection
                                                 }
                                             }

                                         });                 
                             } else invalid_login(res);
	  	    }
	
	  	}); // end connection.execute
    }
  }); // end oracle.connect
}


/////
// Given a set of query results, output a table
//
// res = HTTP result object sent back to the client
// name = Name to query for
// results = List object of query results
function output_user(req, res, email,results, organizedResults) {	
	if(results.length != 0){
	res.render('user/actor',
		   { title:'MockingJay Pennterest',
		   	 user:req.session.user,
		     results: results,		     
		     contents:organizedResults
		 }
	);
	}
	else{
		res.render('user/actor', 
			{ title: "No User Found",
			  results: '',
			  contents: ''
			}
		);
	}

}


function invalid_login(res) {	
	res.render('user/actor', 
		{ title: "No User Found",
		  results: '',
		  boards: '',
		  contents: ''
		}
	);
}


/////
// This is what's called by the main app 
exports.do_work = function(req, res){
	query_db(res,req.query.name);
};

exports.log_in = function(req, res){
	query_db(req, res, req.body.LoginEmail, req.body.password);
};

exports.logIn = function(req, res){
	query_db(req, res,req.session.user.LOGINEMAIL, req.session.user.PASSWORD);
};