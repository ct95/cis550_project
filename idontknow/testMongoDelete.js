var MongoClient = require('mongodb').MongoClient
  , GridStore = require('mongodb').GridStore
  , format = require('util').format;
 
var host = '127.0.0.1';
var port = 27017;
var dbName = 'eric';
 
console.log(">> Connecting to " + host + ":" + port + "/" + dbName);
console.log('mongodb://' + host + ':' + port + '/' + dbName);
 
// Create new file
MongoClient.connect('mongodb://' + host + ':' + port + '/' + dbName, function(err, db) {
     
    if(!err) {
        console.log("We are connected");
    }
 
    var gridStore = new GridStore(db, "foobar", "w");
 
    gridStore.open(function(err, gridStore) {
        gridStore.write("hello world!", function(err, gridStore) {
            GridStore.exist(db, 'foobar', function(err, result) {
                console.log("File 'foobar' exists");
            });
        gridStore.close(function(err, result) {
            // Read the file and dump the contents
            gridStore.read(db, 'foobar', function(err, data) {
                console.log(data);
                db.close()
            });
        });
    });
});
 
// Check that file exists
MongoClient.connect('mongodb://' + host + ':' + port + '/' + dbName, function(err, db) {
 
    //new Gridstore(db, "foobar", "w").open(function(err, gridStore) {
 
        //gridStore.read(db, 'foobar', function(err, data) {
            //console.log(data);
         
         
        //});
    });
});
 
// Delete
MongoClient.connect('mongodb://' + host + ':' + port + '/' + dbName, {native_parser:true}, function(err, db) {
 
    if(!err) {
        console.log("We are connected again");
    }
 
    var gridStore = new GridStore(db, "foobar", "r");
 
    gridStore.open(function(err, gridStore) {
        console.log("gridStore open for deletion");
 
        //gridStore.read(db, 'foobar', function(err, data) {
        //  console.log(data);
 
        GridStore.unlink(db, 'foobar', function(err, gridStore) {
            GridStore.exist(db, 'foobar', function(err, result) {
                console.log("File 'foobar' exists: " + result);
                db.close();
            });
        });
    });
});