var express = require("express"),
    engines = require('consolidate'),
    app = express(),
    dbmessage = '',
    apptitle = 'MockingJay Pennterest',
    MemoryStore = require('connect').session.MemoryStore,
    oracle = require("oracle"),
    connectData = { 
    "hostname": "cis550project.cupvrcsnxzfa.us-west-2.rds.amazonaws.com", 
    "user": "adminuser", 
    "password": "adminuser", 
    "database": "PENNTR" }

var MongoClient = require('mongodb').MongoClient;
var Db = require('mongodb').Db,
Server = require('mongodb').Server,
ReplSetServers = require('mongodb').ReplSetServers,
ObjectID = require('mongodb').ObjectID,
Binary = require('mongodb').Binary,
GridStore = require('mongodb').GridStore,
Grid = require('mongodb').Grid,
Code = require('mongodb').Code,
BSON = require('mongodb').pure().BSON,
assert = require('assert');
var MongoDB = require('mongodb');
var fs = require('fs');
 
var request = require('request');
var http = require('http');
    console.log("set up");


app.configure(function () {
    app.use(express.logger());
    app.use(express.bodyParser());
    app.use(express.methodOverride());

    app.use(express.cookieParser());
    app.use(express.session(
        {secret:"secret key", store:new MemoryStore()}));
    app.use(express.static(__dirname + '/app'));

    app.engine('html', engines.underscore);

    /*
     Set views directory. DO NOT set this with the same static directory!.
     */
    app.set('views', __dirname + '/app/views');
    app.set('view engine', 'jade');
    app.set('PORT', 3030);
    console.log("configured");
});


app.get("/", function (req, res) {
    res.render('index', {
        title:apptitle,
        message:''
    });
    query_db(req, res, req.body.username, req.body.password); 
});

//app.get('/user/registration', function (req, res) {
//    query_db(req, res, req.body.username, req.body.password); 
//});

app.listen(app.get('PORT'));
console.log('Node-Express-MongoDB Login Registration App');
console.log('-------------------------------------------');
console.log("Server Port: " + app.get('PORT'));

function query_db(req, res,userLogin,password) {
    oracle.connect(connectData, function(err, connection) {
        if ( err ) {
            console.log(err);
        } else {
            connection.execute("select * from contents "
                + "where contents.source='mockingJay'",
                   [], 
                   function(err, results) {
                        if ( err ) {
                            console.log(err);
                        } else {
                            output_content(req, res, results);
                            cache_images(res, results);
                        }
                    });
        }
    });    
}

function output_content(req, res, results) {
    console.log('User Data:\n');
    console.log(results);
}

var remoteMongo = 'mongodb://chxuchen:cis550project@ds053688.mongolab.com:53688/mockingjay'

function cache_images(res, results) {
    console.log("Attempting to cache " 
                + results.length + " images");

    MongoClient.connect(remoteMongo, function(err, db) {
    //MongoClient.connect('mongodb://localhost:27017/test', function(err, db) {

        if(!err) {
            console.log("We are connected to Mongo");
        }
        
        var fileId = '';
        // Create a new instance of the gridstore
        //var gridStore = new GridStore(db, 'ourexamplefiletowrite.txt', 'w');
        
        //for (var i = 0; i < results.length; i++) {
            // Open the file
            var i = 10;
            console.log(results[i].CONTENTID + results[i].SOURCE);
            fileId = results[i].CONTENTID + results[i].SOURCE;
            var gridStore =
                new GridStore(db, fileId, 'w');
            console.log("Caching image ...");// + i);

            gridStore.open(function(err, gridStore) { 
                var currentURL = results[i].URL;
                console.log("Attempting to get " + currentURL);
                http.get(currentURL/*results[2].URL*/, function (response) {
                       
                    response.setEncoding('binary');
            
                    var image2 = '';
     
                    console.log('reading data in chunks first');
                    response.on('data', function(chunk) {
                        image2 += chunk;
                        console.log('reading data');
                    });
                                     
                    response.on('end', function() {
                        console.log('done reading data');
                 
                        image1 = new Buffer(image2,"binary");
                                     
                        // Write some data to the file
                        gridStore.write(image1, function(err, gridStore) {
                            assert.equal(null, err);
                 
                            // Close (Flushes the data to MongoDB)
                            gridStore.close(function(err, result) {
                                assert.equal(null, err);
                 
                                GridStore.read(db, fileId, function(err,
                                                             fileData) {
                                    assert.equal(image1.toString('base64'),
                                              fileData.toString('base64'));
                   
                                    console.log('Done, writing local images '
                                        + ' for testing purposes');
                                    console.log("About to open 'cache/" 
                                        + currentURL + "'");
                                    
                                    var extn = currentURL.toString();
                                    var length = (currentURL.length) - 4;
                                    extn = extn.slice(length);
                                    console.log("Extension: " + extn);         
                                    var fd = 
                                        fs.openSync('cache/' + fileId +  extn,
                                         'w');
                 
                                    fs.write(fd, image1, 0, image1.length, 0,
                                        function(err,written) {

                                    });
                                    //display_image(res, "test");
                                });
                            });
                        });             
                    });
                });          
           });
        //}
    });
}

/*
function display_image(res, photos) {

    console.log("about to display image");
    res.render('cachePhoto.jade',
        {
            content: "test"
        }

    );
    

}
*/