
var express = require("express"),
    //routes = require('./routes'),
    //profile = require('./routes/profile'),
    //search = require('./routes/search'),
    //tag = require('./routes/tag'),
    http = require('http'),
    path = require('path'),
    login = require('./app/routes/login'),
    profile = require('./app/routes/profile'),
    tag = require('./app/routes/tag'),
    existingUserCheck = require('./app/routes/existingUserCheck'),
    updateUserInfo = require('./app/routes/updateUserInfo'),
    addRating = require('./app/routes/addRating'),
    addPins = require('./app/routes/addPins'),
    //bing = require('./app/routes/bing'),
    //stylus = require('stylus'),
    //nib = require('nib'),
    // above
    search = require('./app/routes/search'),

    app = express(),
    apptitle = 'MockingJay Pennterest',
    MemoryStore = require('connect').session.MemoryStore,
    oracle = require("oracle"),
    connectData = { 
    "hostname": "cis550project.cupvrcsnxzfa.us-west-2.rds.amazonaws.com", 
    "user": "adminuser", 
    "password": "adminuser", 
    "database": "PENNTR" };


app.configure(function () {
    app.use(express.logger('dev'));
    app.use(express.bodyParser());
    app.use(express.methodOverride());
    //app.use(express.favicon());
    //app.use(express.favicon(__dirname + '/app/img/favicon.ico'));

    app.use(express.cookieParser());
    app.use(express.session(
        {secret:"secret key", store:new MemoryStore()}));
    app.use(express.static(__dirname + '/app'));

    /*
     Set views directory. DO NOT set this with the same static directory!.
     */
    app.set('views', __dirname + '/app/views');
    app.set('view engine', 'jade');
    app.set('PORT', process.env.PORT || 8080);

    if('development' == app.get('env')) {
        app.use(express.errorHandler());
    }
});


app.get("/", function (req, res) {
    if(req.session.user) {
        //console.log('req.session.user = ' + JSON.stringify(req.session.user));
        //console.log('req.session.user.loginEmail = ' + JSON.stringify(req.session.user.LOGINEMAIL));
        res.redirect('/login');
    } else {
        res.render('index', {
        title:apptitle,
        message:''
    });   
    }
});

// app.post("/", function (req, res) {
//     res.render('user/home', {
//         user:req.session.user,
//         title: apptitle
//     })  
// })

// REGISTRATION
app.get('/registration', function (req, res) {
    res.render("user/registration", {
        title:apptitle
    });
});

// Redirect to login
app.get('/login', login.recommendation_loggedIN);

// AUTHENTICATION
app.post('/login', login.recommendation
    //query_db(req, res, req.body.username, req.body.password); 
);

// USER SETTINGS
app.get('/settings', function (req, res) {
    res.render('user/settings', {
        user:req.session.user,
        loginEmail: req.session.user.LOGINEMAIL,
        title:apptitle
    })

});

app.get('/updateUserInfo', updateUserInfo.do_work);

app.post('/addPins', addPins.addPins_updatDB);

// app.get("/user/:name", function (req, res) {
//     if (req.session.loggedIn) {
//         res.render('user/home', {
//             user:req.user,
//             title: apptitle
//         });
//     } else {
//         res.render('index', {
//             title:apptitle,
//             message:''
//         });
//     }
// });

// CREATE USER
// app.post("/create", function (req, res) {

//     console.log('userLogin: ' + req.body.email);
//     console.log('password: ' + req.body.password);
//     console.log('firstName: ' + req.body.firstName);
//     console.log('lastName: ' + req.body.lastName);
//     console.log('affiliation: ' + req.body.affiliation);
//     console.log('interest: ' + req.body.interest);

//     // TO-DO: put this user back to database, set session.user = current user
// });

// Existing User Check
app.get('/existingUserCheck', existingUserCheck.do_work);

// Profile
app.get('/profile', profile.logIn);

// Tag
app.get('/tag', tag.do_work);

// Bing search
//app.post('/bing', bing.bing_search);

// LOGOUT
app.get('/logout', function (req, res) {
    // clear user session
    req.session.user = false;
    res.render('index',{
        title:apptitle,
        message:''});
});

// search
app.post('/search', search.search_tag);

// add ratings
app.post('/addRating', addRating.do_work);

app.listen(app.get('PORT'));
console.log('Node-Express-MongoDB Login Registration App');
console.log('-------------------------------------------');
console.log("Server Port: " + app.get('PORT'));


/////
// Query the oracle database, and call output_profiles on the results
//
// res = HTTP result object sent back to the client
// userLogin = Name to query for
function query_db(req, res,userLogin,password) {
  oracle.connect(connectData, function(err, connection) {
    if ( err ) {
        console.log(err);
    } else {
        // selecting rows - user name
        connection.execute("SELECT firstname FROM users WHERE loginEmail='" + userLogin + 
                "' AND password='" + password+ "' ", 
                   [], 
                   function(err, results) {
                        if ( err ) {
                            console.log(err);
                        } else {
                            req.session.user = results[0];
                            console.log(results);
                            if(results.length > 0) {
                                connection.execute("SELECT b.boardname FROM boards b WHERE b.loginEmail='" + userLogin + "' ", 
                                       [], 
                                       function(err, boardresults) {
                                            if ( err ) {
                                                console.log(err);
                                            } else {
                                                
                                                console.log(boardresults);
                                                if(boardresults.length > 0) {
                                                    output_user(req, res, results, boardresults);

                                                }
                                                connection.close(); // done with the connection
                                            }
                                        });
                
                            }else invalid_login(res);
                        }
                
                    }); // end connection.execute


    }
  }); // end oracle.connect
}

function output_user(req, res, results, boardresults) {
    console.log('User Data:\n');
    console.log(results);
    console.log(results[0]);
    console.log(results[0].FIRSTNAME);

    req.session.loggedIn = true;
    res.render('user/home', { 
        user:results[0],
        title:apptitle,
        boards:boardresults
        });
}

function invalid_login(res) {
    console.log('ERROR: Wrong Username or Password');
    res.render('index', {
        title:apptitle,
        message:'<div class="alert alert-error"><button type="button" class="close" data-dismiss="alert">&times;</button><h4>Error!</h4>Wrong username or password</div>'
        });
}

function authenticate(name, pass, fn) {
    if(!module.parent) console.log('authenticating %s:%s', name, pass);

    oracle.connect(connectData, function(err, connection) {
        console.log('after oracle.connect');
    if ( err ) {
        console.log('inside of if(err)');
        console.log(err);
        return fn(new Error('cannot connect to database'));
    } else {
        // selecting rows
        console.log('before connection.execute');
        connection.execute("SELECT * FROM users WHERE loginEmail='" + name + 
                "' AND password='" + pass +"'", 
                   [], 
                   function(err, user) {
            if ( err ) {
                console.log('inside if(err) after connectin.execute');
                console.log(err);
                return fn(new Error('cannot find user'));
            } else {
                    if(user.length > 0) {// found the user
                        console.log(user);
                        return fn(null, user[0]);
                    }else {
                        console.log('got on else track of statement, but user.length == 0');
                        return fn(new Error('user.length is 0'));
                    }
            }
    
        }); // end connection.execute
    }
  }); // end oracle.connect
}