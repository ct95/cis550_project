// Connect string to Oracle
var connectData = { "hostname": "cis550project.cupvrcsnxzfa.us-west-2.rds.amazonaws.com", "user": "adminuser", "password": "adminuser", "database": "PENNTR" };

var oracle =  require("oracle");

/////
// Query the oracle database, and call output_actors on the results
//
// res = HTTP result object sent back to the client
// tag = Tag to query for
function query_db(res,tag) {
  oracle.connect(connectData, function(err, connection) {
    if ( err ) {
    	console.log(err);
    } else {
	  	// selecting rows
	  	console.log(tag);
	
	    connection.execute("select r.contentid, c.url, avg(r.rating) avgRating " +
	  			"from ratings r, tags t, contents c " +  
	  			"where r.contentid=t.contentid and c.contentid=t.contentid and c.source=t.source and " +
	  			"r.source=t.source and c.contentid=r.contentid and c.source=r.source and t.tag='" + tag + 
	  			"' group by r.contentid, c.url order by avgRating desc",
	  			   [], 
	  			   function(err, tagResults) {
	  	    if ( err ) {
	  	    	console.log(err);
	  	    } else {
	  	    	    if(tagResults.length > 0) {// found the user
	  	    	    	console.log(tagResults);
	  	    	    	output_tagResult(res, tag, tagResults);
                    }else {
                    	invalid_tag(res, tag);
                    }
	  	    }
	
	  	}); // end connection.execute
    }
  }); // end oracle.connect
}

/////
// Given a set of query results, output a table
//
// res = HTTP result object sent back to the client
// tagResults = List object of query results
function output_tagResult(res, tag, tagResults) {	

	if(tagResults.length != 0){
	res.render('search.jade',
		   { title: "Search for tag = " + tag,
		     subTitle: "Match found",
		     urls: tagResults
		 }
	);
	}
	else{
		res.render('search.jade', 
			{ title: "Search for tag = " + tag,
			  subTitle: "No match found!",
			  urls: ''
			}
		);
	}

}

function invalid_tag(res, tag) {	
	res.render('search.jade', 
		{ title: "Search for tag = " + tag,
		  subTitle: "No match found!",
		  urls: ''
		}
	);
}


/////
// This is what's called by the main app 
exports.search_tag = function(req, res){
	console.log('req.body.tag = ' + req.body.tag);
	query_db(res, req.body.tag);
};
