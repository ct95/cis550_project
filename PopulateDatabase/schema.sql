CREATE TABLE Users( 
	loginEmail VARCHAR(255),
	firstName VARCHAR(255),
	lastName VARCHAR(255),
	password VARCHAR(255),	
	affiliation VARCHAR(255),
	interest VARCHAR(255),
	PRIMARY KEY (loginEmail)
);
CREATE TABLE Friends(
	loginEmail VARCHAR(255),
	friendLogin VARCHAR(255),
	PRIMARY KEY (loginEmail, friendLogin),
	FOREIGN KEY (loginEmail) REFERENCES Users,
	FOREIGN KEY (friendLogin) REFERENCES Users
);
CREATE TABLE Boards( 
	loginEmail VARCHAR(255),
	boardName VARCHAR(255),
	PRIMARY KEY (loginEmail, boardName),
	FOREIGN KEY (loginEmail) REFERENCES Users
);
CREATE TABLE Contents( 
	contentID INTEGER,
	source VARCHAR(255),
	type VARCHAR(255),
	url VARCHAR(255),
	PRIMARY KEY (contentID, source)	
);
CREATE TABLE Tags( 
	contentID INTEGER,
	source VARCHAR(255),
	tag VARCHAR(255),
	PRIMARY KEY (contentID, source, tag),
	FOREIGN KEY (contentID, source)	 REFERENCES Contents(contentID,source)
);
CREATE TABLE Pins( 
	contentID INTEGER,
	source VARCHAR(255),
	loginEmail VARCHAR(255),
	boardName VARCHAR(255),	
	PRIMARY KEY (contentID, source, loginEmail, boardName),
	FOREIGN KEY (contentID, source)	 REFERENCES Contents(contentID,source),
	FOREIGN KEY (loginEmail, boardName) REFERENCES  Boards(loginEmail,boardName)
);
CREATE TABLE Ratings(
	contentID INTEGER,
	source VARCHAR(255),
	loginEmail VARCHAR(255),
	rating INTEGER,
	PRIMARY KEY (contentID, source, loginEmail),
	FOREIGN KEY (contentID, source)	 REFERENCES Contents(contentID,source),
	FOREIGN KEY (loginEmail) REFERENCES Users
);
