-- Will

INSERT INTO Users (loginEmail, firstName, lastName, password, affiliation, interest)
VALUES ('wt201@fake.com', 'andy', 'lau', '12345', 'dogLover', 'catFight,pillowFight,couchPotatoing');

INSERT INTO Users (loginEmail, firstName, lastName, password, affiliation, interest)
VALUES ('wt202@fake.com', 'britney', 'ubuntu', '12345', 'penn', 'smile,daydream,sweets');

INSERT INTO Users (loginEmail, firstName, lastName, password, affiliation, interest)
VALUES ('wt203@fake.com', 'chandler', 'bin', '12345', 'tennis', 'movie,stapleThingsToMyBossHead');

INSERT INTO Users (loginEmail, firstName, lastName, password, affiliation, interest)
VALUES ('wt204@fake.com', 'diane', 'drowsy', '12345', 'insomnia', 'sleep,eat,chocolate');

INSERT INTO Users (loginEmail, firstName, lastName, password, affiliation, interest)
VALUES ('wt205@fake.com', 'eddie', 'pho', '12345', 'chef', 'opera,photography,history');

INSERT INTO Users (loginEmail, firstName, lastName, password, affiliation, interest)
VALUES ('wt206@fake.com', 'fran', 'Drescher', '12345', 'Sheffield', 'babysitting,flirtingWithMrSheffield');

INSERT INTO Users (loginEmail, firstName, lastName, password, affiliation, interest)
VALUES ('wt207@fake.com', 'geoff', 'Maji', '12345', 'LAPD', 'music,windowShopping,beatBoxing,makingNoise');

INSERT INTO Users (loginEmail, firstName, lastName, password, affiliation, interest)
VALUES ('wt208@fake.com', 'hilton', 'bonjure', '12345', 'hilton', 'shopping,yawning,singing');

INSERT INTO Users (loginEmail, firstName, lastName, password, affiliation, interest)
VALUES ('wt209@fake.com', 'ima', 'jin', '12345', 'insomnia', 'daydreaming,cooking');

INSERT INTO Users (loginEmail, firstName, lastName, password, affiliation, interest)
VALUES ('wt210@fake.com', 'josh', 'brown', '12345', 'ucsd', 'chemistry,math,computer,apple');

-----------------------------------------------------------------------------------

INSERT INTO Friends (loginEmail, friendLogin)
VALUES ('wt201@fake.com', 'wt202@fake.com');

INSERT INTO Friends (loginEmail, friendLogin)
VALUES ('wt201@fake.com', 'wt203@fake.com');

INSERT INTO Friends (loginEmail, friendLogin)
VALUES ('wt201@fake.com', 'wt204@fake.com');

INSERT INTO Friends (loginEmail, friendLogin)
VALUES ('wt201@fake.com', 'wt205@fake.com');

INSERT INTO Friends (loginEmail, friendLogin)
VALUES ('wt201@fake.com', 'wt206@fake.com');

INSERT INTO Friends (loginEmail, friendLogin)
VALUES ('wt201@fake.com', 'wt207@fake.com');

INSERT INTO Friends (loginEmail, friendLogin)
VALUES ('wt201@fake.com', 'wt208@fake.com');

INSERT INTO Friends (loginEmail, friendLogin)
VALUES ('wt201@fake.com', 'wt209@fake.com');

INSERT INTO Friends (loginEmail, friendLogin)
VALUES ('wt201@fake.com', 'wt210@fake.com');

INSERT INTO Friends (loginEmail, friendLogin)
VALUES ('wt202@fake.com', 'wt205@fake.com');

INSERT INTO Friends (loginEmail, friendLogin)
VALUES ('wt202@fake.com', 'wt207@fake.com');

INSERT INTO Friends (loginEmail, friendLogin)
VALUES ('wt202@fake.com', 'wt209@fake.com');

INSERT INTO Friends (loginEmail, friendLogin)
VALUES ('wt203@fake.com', 'wt205@fake.com');

INSERT INTO Friends (loginEmail, friendLogin)
VALUES ('wt203@fake.com', 'wt206@fake.com');

INSERT INTO Friends (loginEmail, friendLogin)
VALUES ('wt203@fake.com', 'wt208@fake.com');

INSERT INTO Friends (loginEmail, friendLogin)
VALUES ('wt204@fake.com', 'wt206@fake.com');

INSERT INTO Friends (loginEmail, friendLogin)
VALUES ('wt204@fake.com', 'wt208@fake.com');

INSERT INTO Friends (loginEmail, friendLogin)
VALUES ('wt204@fake.com', 'wt210@fake.com');

INSERT INTO Friends (loginEmail, friendLogin)
VALUES ('wt205@fake.com', 'wt206@fake.com');

INSERT INTO Friends (loginEmail, friendLogin)
VALUES ('wt205@fake.com', 'wt207@fake.com');

INSERT INTO Friends (loginEmail, friendLogin)
VALUES ('wt205@fake.com', 'wt209@fake.com');

INSERT INTO Friends (loginEmail, friendLogin)
VALUES ('wt206@fake.com', 'wt209@fake.com');

INSERT INTO Friends (loginEmail, friendLogin)
VALUES ('wt206@fake.com', 'wt210@fake.com');

INSERT INTO Friends (loginEmail, friendLogin)
VALUES ('wt207@fake.com', 'wt209@fake.com');

INSERT INTO Friends (loginEmail, friendLogin)
VALUES ('wt208@fake.com', 'wt209@fake.com');

INSERT INTO Friends (loginEmail, friendLogin)
VALUES ('wt209@fake.com', 'wt210@fake.com');

------------------------------------------------------------------------------

INSERT INTO Boards (loginEmail, boardName)
VALUES ('wt201@fake.com', 'AndyBoard1');

INSERT INTO Boards (loginEmail, boardName)
VALUES ('wt201@fake.com', 'AndyBoard2');

INSERT INTO Boards (loginEmail, boardName)
VALUES ('wt202@fake.com', 'BritneyBoard1');

INSERT INTO Boards (loginEmail, boardName)
VALUES ('wt202@fake.com', 'BritneyBoard2');

INSERT INTO Boards (loginEmail, boardName)
VALUES ('wt202@fake.com', 'BritneyBoard3');

INSERT INTO Boards (loginEmail, boardName)
VALUES ('wt203@fake.com', 'ChandlerBoard1');

INSERT INTO Boards (loginEmail, boardName)
VALUES ('wt204@fake.com', 'DianeBoard1');

INSERT INTO Boards (loginEmail, boardName)
VALUES ('wt205@fake.com', 'EddieBoard1');

INSERT INTO Boards (loginEmail, boardName)
VALUES ('wt206@fake.com', 'FranBoard1');

INSERT INTO Boards (loginEmail, boardName)
VALUES ('wt207@fake.com', 'GeoffBoard1');

INSERT INTO Boards (loginEmail, boardName)
VALUES ('wt207@fake.com', 'GeoffBoard2');

INSERT INTO Boards (loginEmail, boardName)
VALUES ('wt208@fake.com', 'HiltonBoard1');

INSERT INTO Boards (loginEmail, boardName)
VALUES ('wt208@fake.com', 'HiltonBoard2');

INSERT INTO Boards (loginEmail, boardName)
VALUES ('wt209@fake.com', 'ImaBoard1');

INSERT INTO Boards (loginEmail, boardName)
VALUES ('wt209@fake.com', 'ImaBoard2');

INSERT INTO Boards (loginEmail, boardName)
VALUES ('wt210@fake.com', 'JoshBoard1');

------------------------------------------------------------------------

INSERT INTO Contents (contentID, source, type, url)
VALUES (201, 'mockingJay', 'photo', 'http://static4.wikia.nocookie.net/__cb20130806035146/despicableme/images/1/1b/Check-in-minion.jpg');

INSERT INTO Contents (contentID, source, type, url)
VALUES (202, 'mockingJay', 'photo', 'http://www.openentrance.com/wp-content/uploads/2007/06/fiona-062807.jpg');

INSERT INTO Contents (contentID, source, type, url)
VALUES (203, 'mockingJay', 'photo', 'http://static2.wikia.nocookie.net/__cb20091230233251/shrek/images/d/d1/Puss_in_Boots_1.jpg');

INSERT INTO Contents (contentID, source, type, url)
VALUES (204, 'mockingJay', 'photo', 'http://www2.csdm.qc.ca/slg/activites/PORTFOLIO/2007_2008/63_ana_maria/63_soupe_Ana-maria/images/nemo.jpg');

INSERT INTO Contents (contentID, source, type, url)
VALUES (205, 'mockingJay', 'photo', 'http://www.picgifs.com/graphics/s/shrek/graphics-shrek-335572.jpg');

INSERT INTO Contents (contentID, source, type, url)
VALUES (206, 'mockingJay', 'photo', 'http://blog.zap2it.com/pop2it/alice_in_wonderland_red_queen.jpg');

INSERT INTO Contents (contentID, source, type, url)
VALUES (207, 'mockingJay', 'photo', 'http://cdn.crushable.com/files/2012/06/2006_devil_wears_prada_007.jpg');

INSERT INTO Contents (contentID, source, type, url)
VALUES (208, 'mockingJay', 'photo', 'http://www.fashionverbatim.com/wp-content/uploads/2013/03/fashion-anna-wintour.jpeg-1280x960.jpg');

INSERT INTO Contents (contentID, source, type, url)
VALUES (209, 'mockingJay', 'photo', 'http://images2.fanpop.com/image/photos/9600000/Lily-Stills-lily-aldrin-9609355-334-500.jpg');

INSERT INTO Contents (contentID, source, type, url)
VALUES (210, 'mockingJay', 'photo', 'http://pixel.nymag.com/imgs/daily/vulture/2012/10/12/12-cobie-smulders-robin-sparkes.o.jpg/a_560x375.jpg');


------------------------------------------------------------------------

INSERT INTO Tags (contentID, source, tag)
VALUES (201, 'mockingJay', 'minion');

INSERT INTO Tags (contentID, source, tag)
VALUES (202, 'mockingJay', 'fiona');

INSERT INTO Tags (contentID, source, tag)
VALUES (203, 'mockingJay', 'PussInBoots');

INSERT INTO Tags (contentID, source, tag)
VALUES (204, 'mockingJay', 'nemo');

INSERT INTO Tags (contentID, source, tag)
VALUES (205, 'mockingJay', 'shrek');

INSERT INTO Tags (contentID, source, tag)
VALUES (206, 'mockingJay', 'RedQueen');

INSERT INTO Tags (contentID, source, tag)
VALUES (207, 'mockingJay', 'MirandaPriestly');

INSERT INTO Tags (contentID, source, tag)
VALUES (208, 'mockingJay', 'annaWintour');

INSERT INTO Tags (contentID, source, tag)
VALUES (209, 'mockingJay', 'AlysonHannigan');

INSERT INTO Tags (contentID, source, tag)
VALUES (210, 'mockingJay', 'CobieSmulders');

---------------------------------------------------------------------

INSERT INTO Pins (contentID, source, loginEmail, boardName)
VALUES (201, 'mockingJay', 'wt201@fake.com', 'AndyBoard1');

INSERT INTO Pins (contentID, source, loginEmail, boardName)
VALUES (201, 'mockingJay', 'wt202@fake.com', 'BritneyBoard1');

INSERT INTO Pins (contentID, source, loginEmail, boardName)
VALUES (201, 'mockingJay', 'wt203@fake.com', 'ChandlerBoard1');

INSERT INTO Pins (contentID, source, loginEmail, boardName)
VALUES (202, 'mockingJay', 'wt201@fake.com', 'AndyBoard2');

INSERT INTO Pins (contentID, source, loginEmail, boardName)
VALUES (202, 'mockingJay', 'wt210@fake.com', 'JoshBoard1');

INSERT INTO Pins (contentID, source, loginEmail, boardName)
VALUES (203, 'mockingJay', 'wt207@fake.com', 'GeoffBoard1');

INSERT INTO Pins (contentID, source, loginEmail, boardName)
VALUES (203, 'mockingJay', 'wt208@fake.com', 'HiltonBoard2');

INSERT INTO Pins (contentID, source, loginEmail, boardName)
VALUES (203, 'mockingJay', 'wt209@fake.com', 'ImaBoard1');

INSERT INTO Pins (contentID, source, loginEmail, boardName)
VALUES (204, 'mockingJay', 'wt204@fake.com', 'DianeBoard1');

INSERT INTO Pins (contentID, source, loginEmail, boardName)
VALUES (205, 'mockingJay', 'wt202@fake.com', 'BritneyBoard2');

INSERT INTO Pins (contentID, source, loginEmail, boardName)
VALUES (205, 'mockingJay', 'wt206@fake.com', 'FranBoard1');

INSERT INTO Pins (contentID, source, loginEmail, boardName)
VALUES (205, 'mockingJay', 'wt204@fake.com', 'DianeBoard1');

INSERT INTO Pins (contentID, source, loginEmail, boardName)
VALUES (206, 'mockingJay', 'wt202@fake.com', 'BritneyBoard3');

INSERT INTO Pins (contentID, source, loginEmail, boardName)
VALUES (207, 'mockingJay', 'wt208@fake.com', 'HiltonBoard1');

INSERT INTO Pins (contentID, source, loginEmail, boardName)
VALUES (207, 'mockingJay', 'wt205@fake.com', 'EddieBoard1');

INSERT INTO Pins (contentID, source, loginEmail, boardName)
VALUES (207, 'mockingJay', 'wt209@fake.com', 'ImaBoard2');

INSERT INTO Pins (contentID, source, loginEmail, boardName)
VALUES (208, 'mockingJay', 'wt202@fake.com', 'BritneyBoard2');

INSERT INTO Pins (contentID, source, loginEmail, boardName)
VALUES (208, 'mockingJay', 'wt203@fake.com', 'ChandlerBoard1');

INSERT INTO Pins (contentID, source, loginEmail, boardName)
VALUES (209, 'mockingJay', 'wt207@fake.com', 'GeoffBoard2');

INSERT INTO Pins (contentID, source, loginEmail, boardName)
VALUES (209, 'mockingJay', 'wt206@fake.com', 'FranBoard1');

INSERT INTO Pins (contentID, source, loginEmail, boardName)
VALUES (210, 'mockingJay', 'wt202@fake.com', 'BritneyBoard3');

----------------------------------------------------------------------------------------

INSERT INTO Ratings (contentID, source, loginEmail, rating)
VALUES (201, 'mockingJay', 'wt201@fake.com', 5);

INSERT INTO Ratings (contentID, source, loginEmail, rating)
VALUES (201, 'mockingJay', 'wt202@fake.com', 2);

INSERT INTO Ratings (contentID, source, loginEmail, rating)
VALUES (201, 'mockingJay', 'wt203@fake.com', 1);

INSERT INTO Ratings (contentID, source, loginEmail, rating)
VALUES (202, 'mockingJay', 'wt201@fake.com', 2);

INSERT INTO Ratings (contentID, source, loginEmail, rating)
VALUES (202, 'mockingJay', 'wt210@fake.com', 3);

INSERT INTO Ratings (contentID, source, loginEmail, rating)
VALUES (203, 'mockingJay', 'wt207@fake.com', 4);

INSERT INTO Ratings (contentID, source, loginEmail, rating)
VALUES (203, 'mockingJay', 'wt208@fake.com', 1);

INSERT INTO Ratings (contentID, source, loginEmail, rating)
VALUES (203, 'mockingJay', 'wt209@fake.com', 5);

INSERT INTO Ratings (contentID, source, loginEmail, rating)
VALUES (204, 'mockingJay', 'wt204@fake.com', 5);

INSERT INTO Ratings (contentID, source, loginEmail, rating)
VALUES (205, 'mockingJay', 'wt202@fake.com', 3);

INSERT INTO Ratings (contentID, source, loginEmail, rating)
VALUES (205, 'mockingJay', 'wt206@fake.com', 2);

INSERT INTO Ratings (contentID, source, loginEmail, rating)
VALUES (205, 'mockingJay', 'wt204@fake.com', 5);

INSERT INTO Ratings (contentID, source, loginEmail, rating)
VALUES (206, 'mockingJay', 'wt202@fake.com', 4);

INSERT INTO Ratings (contentID, source, loginEmail, rating)
VALUES (207, 'mockingJay', 'wt208@fake.com', 1);

INSERT INTO Ratings (contentID, source, loginEmail, rating)
VALUES (207, 'mockingJay', 'wt205@fake.com', 2);

INSERT INTO Ratings (contentID, source, loginEmail, rating)
VALUES (207, 'mockingJay', 'wt209@fake.com', 5);

INSERT INTO Ratings (contentID, source, loginEmail, rating)
VALUES (208, 'mockingJay', 'wt202@fake.com', 2);

INSERT INTO Ratings (contentID, source, loginEmail, rating)
VALUES (208, 'mockingJay', 'wt203@fake.com', 1);

INSERT INTO Ratings (contentID, source, loginEmail, rating)
VALUES (209, 'mockingJay', 'wt207@fake.com', 5);

INSERT INTO Ratings (contentID, source, loginEmail, rating)
VALUES (209, 'mockingJay', 'wt206@fake.com', 2);

INSERT INTO Ratings (contentID, source, loginEmail, rating)
VALUES (210, 'mockingJay', 'wt202@fake.com', 2);