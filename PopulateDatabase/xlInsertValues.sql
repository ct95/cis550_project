--Xuan

INSERT INTO Users (loginEmail, firstName, lastName, password, affiliation, interest)
VALUES ('tc@drexel.edu', 'Tim', 'Couch', '12345', 'Drexel', 'Boxing');

INSERT INTO Users (loginEmail, firstName, lastName, password, affiliation, interest)
VALUES ('ra@upenn.edu', 'Ryan', 'Allen', '12345', 'Upenn', 'coding');

INSERT INTO Users (loginEmail, firstName, lastName, password, affiliation, interest)
VALUES ('ms@temple.edu', 'Michelle', 'Smith', '12345', 'Temple', 'Drawing');

INSERT INTO Users (loginEmail, firstName, lastName, password, affiliation, interest)
VALUES ('ma@freelibrary.org', 'Mark', 'Allen', '12345', 'Free Library', 'reading');

INSERT INTO Users (loginEmail, firstName, lastName, password, affiliation, interest)
VALUES ('bl@wharton.upenn.edu', 'Bob', 'Lemmon', '12345', 'Wharton School', 'Lecturing');

INSERT INTO Users (loginEmail, firstName, lastName, password, affiliation, interest)
values ('cc@philyart.org', 'Charlse', 'Couch', '12345', 'Philadelphia Art Museum', 'drawing');

INSERT INTO Users (loginEmail, firstName, lastName, password, affiliation, interest)
values ('ts@wharton.upenn.edu', 'Tom', 'Spalding', '12345', 'Wharton School', 'coding');

INSERT INTO Users (loginEmail, firstName, lastName, password, affiliation, interest)
values ('tw@upenn.edu', 'Ted', 'White', '12345', 'Upenn', 'running'); 

INSERT INTO Users (loginEmail, firstName, lastName, password, affiliation, interest)
values ('kn@temple.edu', 'Kevin', 'Ney', '12345', 'Temple', 'fencing');

INSERT INTO Users (loginEmail, firstName, lastName, password, affiliation, interest)
values ('jw@temple.edu', 'James', 'Wilson', '12345', 'Temple', 'Play football');

-----------------------------------------------------

INSERT INTO Friends (loginEmail, friendLogin)
VALUES ('cc@philyart.org', 'bl@wharton.upenn.edu');

INSERT INTO Friends (loginEmail, friendLogin)
VALUES ('tc@drexel.edu', 'ra@upenn.edu');

INSERT INTO Friends (loginEmail, friendLogin)
VALUES ('ms@temple.edu', 'ma@freelibrary.org');

INSERT INTO Friends (loginEmail, friendLogin)
VALUES ('ms@temple.edu','bl@wharton.upenn.edu');

-----------------------------------------------------


INSERT INTO Boards (loginEmail, boardName)
VALUES ('cc@philyart.org', 'CharlseBoard1');

INSERT INTO Boards (loginEmail, boardName)
VALUES ('cc@philyart.org', 'CharlseBoard2');

INSERT INTO Boards (loginEmail, boardName)
VALUES ('tc@drexel.edu', 'TimBoard1');

INSERT INTO Boards (loginEmail, boardName)
VALUES ('tc@drexel.edu', 'TimBoard2');

INSERT INTO Boards (loginEmail, boardName)
VALUES ('ra@upenn.edu', 'RyanBoard1');

INSERT INTO Boards (loginEmail, boardName)
VALUES ('ms@temple.edu', 'MichelleBoard1');

INSERT INTO Boards (loginEmail, boardName)
VALUES ('ma@freelibrary.org', 'MarkBoard1');

INSERT INTO Boards (loginEmail, boardName)
VALUES ('bl@wharton.upenn.edu', 'BobBoard1');

INSERT INTO Boards (loginEmail, boardName)
VALUES ('bl@wharton.upenn.edu', 'BobBoard2');

INSERT INTO Boards (loginEmail, boardName)
VALUES ('ts@wharton.upenn.edu', 'TomBoard1');

INSERT INTO Boards (loginEmail, boardName)
VALUES ('tw@upenn.edu', 'TedBoard1');

INSERT INTO Boards (loginEmail, boardName)
VALUES ('kn@temple.edu', 'KevinBoard1');

INSERT INTO Boards (loginEmail, boardName)
VALUES ('jw@temple.edu', 'JamesBoard1');

------------------------------------------------------


INSERT INTO Contents (contentID, source, type, url)
VALUES (401, 'mockingJay', 'photo', 'http://blogs-images.forbes.com/brucedorminey/files/2012/04/Copernicus_NTR_LEO.2k.jpg');

INSERT INTO Contents (contentID, source, type, url)
VALUES (402, 'mockingJay', 'photo', 'http://graphics8.nytimes.com/images/2012/04/27/arts/JPTERRACOTTA1/JPTERRACOTTA1-articleLarge.jpg');

INSERT INTO Contents (contentID, source, type, url)
VALUES (403, 'mockingJay', 'photo', 'http://media.npr.org/assets/img/2012/01/04/ap99121501386_custom-feedbb6efa738efee47e7828e805758dc427fa60-s6-c30.jpg');

INSERT INTO Contents (contentID, source, type, url)
VALUES (404, 'mockingJay', 'photo', 'http://www.familygonefree.com/wp-content/uploads/2012/09/longwood-gardens.jpeg');

INSERT INTO Contents (contentID, source, type, url)
VALUES (405, 'mockingJay', 'photo', 'http://www.nasa.gov/sites/default/files/images/718518main_loresworm.jpg');

INSERT INTO Contents (contentID, source, type, url)
VALUES (406, 'mockingJay', 'photo', 'http://static.ddmcdn.com/gif/blogs/6a00d8341bf67c53ef017d3e524a67970c-800wi.jpg');

INSERT INTO Contents (contentID, source, type, url)
VALUES (407, 'mockingJay', 'photo', 
'http://screenshots.en.sftcdn.net/en/scrn/73000/73662/national-geographic-polar-animals-screensaver-7.jpg');

INSERT INTO Contents (contentID, source, type, url)
VALUES (408, 'mockingJay', 'photo', 'http://upload.wikimedia.org/wikipedia/commons/d/d8/NASA_Mars_Rover.jpg');


-------------------------------------------------------


INSERT INTO Tags (contentID, source, tag)
VALUES (401, 'mockingJay', 'NASA');

INSERT INTO Tags (contentID, source, tag)
VALUES (401, 'mockingJay', 'Computer');

INSERT INTO Tags (contentID, source, tag)
VALUES (401, 'mockingJay', 'simulation');

INSERT INTO Tags (contentID, source, tag)
VALUES (402, 'mockingJay', 'discovery');

INSERT INTO Tags (contentID, source, tag)
VALUES (402, 'mockingJay', 'Warriors');

INSERT INTO Tags (contentID, source, tag)
VALUES (403, 'mockingJay', 'sichuan');

INSERT INTO Tags (contentID, source, tag)
VALUES (403, 'mockingJay', 'panda');

INSERT INTO Tags (contentID, source, tag)
VALUES (404, 'mockingJay', 'phily');

INSERT INTO Tags (contentID, source, tag)
VALUES (404, 'mockingJay', 'longwood');

INSERT INTO Tags (contentID, source, tag)
VALUES (404, 'mockingJay', 'garden');

INSERT INTO Tags (contentID, source, tag)
VALUES (405, 'mockingJay', 'nasa');

INSERT INTO Tags (contentID, source, tag)
VALUES (405, 'mockingJay', 'star');

INSERT INTO Tags (contentID, source, tag)
VALUES (406, 'mockingJay', 'discovery');

INSERT INTO Tags (contentID, source, tag)
VALUES (406, 'mockingJay', 'immortal');

INSERT INTO Tags (contentID, source, tag)
VALUES (406, 'mockingJay', 'jellyfish');

INSERT INTO Tags (contentID, source, tag)
VALUES (407, 'mockingJay', 'geo');

INSERT INTO Tags (contentID, source, tag)
VALUES (407, 'mockingJay', 'polar');

INSERT INTO Tags (contentID, source, tag)
VALUES (407, 'mockingJay', 'animal');

INSERT INTO Tags (contentID, source, tag)
VALUES (408, 'mockingJay', 'NASA');

INSERT INTO Tags (contentID, source, tag)
VALUES (408, 'mockingJay', 'Mars');

INSERT INTO Tags (contentID, source, tag)
VALUES (408, 'mockingJay', 'rover');


------------------------------------------------------


INSERT INTO Pins (contentID, source, loginEmail, boardName)
VALUES (401, 'mockingJay', 'cc@philyart.org', 'CharlseBoard1');

INSERT INTO Pins (contentID, source, loginEmail, boardName)
VALUES (402, 'mockingJay', 'cc@philyart.org', 'CharlseBoard2');

INSERT INTO Pins (contentID, source, loginEmail, boardName)
VALUES (403,'mockingJay','tc@drexel.edu', 'TimBoard1');

INSERT INTO Pins (contentID, source, loginEmail, boardName)
VALUES (404, 'mockingJay', 'tc@drexel.edu', 'TimBoard1'); 


INSERT INTO Pins (contentID, source, loginEmail, boardName)
VALUES (405,'mockingJay', 'tc@drexel.edu', 'TimBoard1'); 

INSERT INTO Pins (contentID, source, loginEmail, boardName)
VALUES (406,'mockingJay','tc@drexel.edu', 'TimBoard2'); 

INSERT INTO Pins (contentID, source, loginEmail, boardName)
VALUES (407,'mockingJay','tc@drexel.edu', 'TimBoard2'); 

INSERT INTO Pins (contentID, source, loginEmail, boardName)
VALUES (408,'mockingJay','ra@upenn.edu', 'RyanBoard1'); 

INSERT INTO Pins (contentID, source, loginEmail, boardName)
VALUES (401,'mockingJay','ra@upenn.edu', 'RyanBoard1'); 

INSERT INTO Pins (contentID, source, loginEmail, boardName)
VALUES (402,'mockingJay', 'ra@upenn.edu', 'RyanBoard1'); 

INSERT INTO Pins (contentID, source, loginEmail, boardName)
VALUES (403,'mockingJay','ms@temple.edu', 'MichelleBoard1'); 

INSERT INTO Pins (contentID, source, loginEmail, boardName)
VALUES (403,'mockingJay', 'ma@freelibrary.org', 'MarkBoard1');

INSERT INTO Pins (contentID, source, loginEmail, boardName)
VALUES (404,'mockingJay','ma@freelibrary.org', 'MarkBoard1'); 

INSERT INTO Pins (contentID, source, loginEmail, boardName)
VALUES (405,'mockingJay','bl@wharton.upenn.edu', 'BobBoard1'); 

INSERT INTO Pins (contentID, source, loginEmail, boardName)
VALUES (406,'mockingJay','bl@wharton.upenn.edu', 'BobBoard1'); 

INSERT INTO Pins (contentID, source, loginEmail, boardName)
VALUES (407,'mockingJay','bl@wharton.upenn.edu', 'BobBoard2'); 

INSERT INTO Pins (contentID, source, loginEmail, boardName)
VALUES (408,'mockingJay','bl@wharton.upenn.edu', 'BobBoard2'); 

INSERT INTO Pins (contentID, source, loginEmail, boardName)
VALUES (408,'mockingJay','ts@wharton.upenn.edu', 'TomBoard1');


INSERT INTO Pins (contentID, source, loginEmail, boardName)
VALUES (408,'mockingJay','tw@upenn.edu', 'TedBoard1');

INSERT INTO Pins (contentID, source, loginEmail, boardName)
VALUES (408,'mockingJay','kn@temple.edu', 'KevinBoard1');

INSERT INTO Pins (contentID, source, loginEmail, boardName)
VALUES (408,'mockingJay','jw@temple.edu', 'JamesBoard1');


--------------------------------------------------------


INSERT INTO Ratings (contentID, source, loginEmail, rating)
VALUES (401, 'mockingJay', 'tc@drexel.edu', 5);

INSERT INTO Ratings (contentID, source, loginEmail, rating)
VALUES (402, 'mockingJay', 'tc@drexel.edu', 2);

INSERT INTO Ratings (contentID, source, loginEmail, rating)
VALUES (403, 'mockingJay', 'ra@upenn.edu', 5);

INSERT INTO Ratings (contentID, source, loginEmail, rating)
VALUES (404, 'mockingJay', 'ra@upenn.edu', 5);                   

INSERT INTO Ratings (contentID, source, loginEmail, rating)
VALUES (405, 'mockingJay', 'ra@upenn.edu', 2);                   

INSERT INTO Ratings (contentID, source, loginEmail, rating)
VALUES (406, 'mockingJay', 'ms@temple.edu', 5);  

INSERT INTO Ratings (contentID, source, loginEmail, rating)
VALUES (407, 'mockingJay', 'ms@temple.edu', 5);                   

INSERT INTO Ratings (contentID, source, loginEmail, rating)
VALUES (408, 'mockingJay', 'ma@freelibrary.org', 2);                   

INSERT INTO Ratings (contentID, source, loginEmail, rating)
VALUES (404, 'mockingJay', 'ma@freelibrary.org', 5);  

INSERT INTO Ratings (contentID, source, loginEmail, rating)
VALUES (401, 'mockingJay', 'bl@wharton.upenn.edu', 5);                   

INSERT INTO Ratings (contentID, source, loginEmail, rating)
VALUES (402, 'mockingJay', 'bl@wharton.upenn.edu', 2);                   

INSERT INTO Ratings (contentID, source, loginEmail, rating)
VALUES (403, 'mockingJay', 'cc@philyart.org', 5);  

