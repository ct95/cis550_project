--Chuya

INSERT INTO Users (loginEmail, firstName, lastName, password, affiliation, interest)
VALUES ('ma@downtonabbey.org', 'Mary', 'Crawley', '12345', 'Downton', 'reading,writing,walking,matthew');

INSERT INTO Users (loginEmail, firstName, lastName, password, affiliation, interest)
VALUES ('ed@downtonabbey.org', 'Edith', 'Crawley', '12345', 'Downton', 'reading,walking,riding,gardening');

INSERT INTO Users (loginEmail, firstName, lastName, password, affiliation, interest)
VALUES ('sy@downtonabbey.org', 'Sybil', 'Crawley', '12345', 'Downton', 'reading,writing,working');

INSERT INTO Users (loginEmail, firstName, lastName, password, affiliation, interest)
VALUES ('co@downtonabbey.org', 'Cora', 'Crawley', '12345', 'Downton', 'shopping,prettyDresses,flowers,daughters');

INSERT INTO Users (loginEmail, firstName, lastName, password, affiliation, interest)
VALUES ('ro@downtonabbey.org', 'Robert', 'Crawley', '12345', 'Downton', 'riding,reading,isis,dogs');

INSERT INTO Users (loginEmail, firstName, lastName, password, affiliation, interest)
VALUES ('vi@downtonabbey.org', 'Violet', 'Crawley', '12345', 'Downton', 'reading,gardening,jokes,tradition');

INSERT INTO Users (loginEmail, firstName, lastName, password, affiliation, interest)
VALUES ('is@downtonabbey.org', 'Isabel', 'Crawley', '12345', 'Downton', 'reading,changes,volunteer');

INSERT INTO Users (loginEmail, firstName, lastName, password, affiliation, interest)
VALUES ('mat@downtonabbey.org', 'Matthew', 'Crawley', '12345', 'Downton', 'reading,writing,working,mary,church,bicycles');


-----------------------------------------------------

-- Mary's Friend
INSERT INTO Friends (loginEmail, friendLogin)
VALUES ('ma@downtonabbey.org', 'sy@downtonabbey.org');

INSERT INTO Friends (loginEmail, friendLogin)
VALUES ('ma@downtonabbey.org', 'mat@downtonabbey.org');

-- Edith's Friend
INSERT INTO Friends (loginEmail, friendLogin)
VALUES ('ed@downtonabbey.org', 'sy@downtonabbey.org');

-- Sybil's Friend
INSERT INTO Friends (loginEmail, friendLogin)
VALUES ('sy@downtonabbey.org', 'ed@downtonabbey.org');

INSERT INTO Friends (loginEmail, friendLogin)
VALUES ('sy@downtonabbey.org', 'ma@downtonabbey.org');

INSERT INTO Friends (loginEmail, friendLogin)
VALUES ('sy@downtonabbey.org', 'mat@downtonabbey.org');

INSERT INTO Friends (loginEmail, friendLogin)
VALUES ('sy@downtonabbey.org', 'co@downtonabbey.org');

-- Cora's Friend
INSERT INTO Friends (loginEmail, friendLogin)
VALUES ('co@downtonabbey.org', 'ma@downtonabbey.org');

INSERT INTO Friends (loginEmail, friendLogin)
VALUES ('co@downtonabbey.org', 'ed@downtonabbey.org');

INSERT INTO Friends (loginEmail, friendLogin)
VALUES ('co@downtonabbey.org', 'sy@downtonabbey.org');

INSERT INTO Friends (loginEmail, friendLogin)
VALUES ('co@downtonabbey.org', 'ro@downtonabbey.org');

-- Rober's Friend
INSERT INTO Friends (loginEmail, friendLogin)
VALUES ('ro@downtonabbey.org', 'ma@downtonabbey.org');

INSERT INTO Friends (loginEmail, friendLogin)
VALUES ('ro@downtonabbey.org', 'co@downtonabbey.org');

INSERT INTO Friends (loginEmail, friendLogin)
VALUES ('ro@downtonabbey.org', 'mat@downtonabbey.org');

-- Violet's Friend
INSERT INTO Friends (loginEmail, friendLogin)
VALUES ('vi@downtonabbey.org', 'ma@downtonabbey.org');

INSERT INTO Friends (loginEmail, friendLogin)
VALUES ('vi@downtonabbey.org', 'ro@downtonabbey.org');

INSERT INTO Friends (loginEmail, friendLogin)
VALUES ('vi@downtonabbey.org', 'is@downtonabbey.org');

-- Isabel'S Friend
INSERT INTO Friends (loginEmail, friendLogin)
VALUES ('is@downtonabbey.org', 'ma@downtonabbey.org');

INSERT INTO Friends (loginEmail, friendLogin)
VALUES ('is@downtonabbey.org', 'mat@downtonabbey.org');

INSERT INTO Friends (loginEmail, friendLogin)
VALUES ('is@downtonabbey.org', 'vi@downtonabbey.org');

-- Matthew's Friend
INSERT INTO Friends (loginEmail, friendLogin)
VALUES ('mat@downtonabbey.org', 'ma@downtonabbey.org');

INSERT INTO Friends (loginEmail, friendLogin)
VALUES ('mat@downtonabbey.org', 'is@downtonabbey.org');

INSERT INTO Friends (loginEmail, friendLogin)
VALUES ('mat@downtonabbey.org', 'ro@downtonabbey.org');


-----------------------------------------------------

-- Mary's board
INSERT INTO Boards (loginEmail, boardName)
VALUES ('ma@downtonabbey.org', 'Mary Board 1');

INSERT INTO Boards (loginEmail, boardName)
VALUES ('ma@downtonabbey.org', 'Mary Board 2');

-- Edith's board
INSERT INTO Boards (loginEmail, boardName)
VALUES ('ed@downtonabbey.org', 'Edith Board 1');

-- Sybil's board
INSERT INTO Boards (loginEmail, boardName)
VALUES ('sy@downtonabbey.org', 'Sybil Board 1');

-- Cora's board
INSERT INTO Boards (loginEmail, boardName)
VALUES ('co@downtonabbey.org', 'Cora Board 1');

-- Robert's board
INSERT INTO Boards (loginEmail, boardName)
VALUES ('ro@downtonabbey.org', 'Robert Board 1');

-- Violet's board
INSERT INTO Boards (loginEmail, boardName)
VALUES ('vi@downtonabbey.org', 'Violet Board 1');

-- Isabel's board
INSERT INTO Boards (loginEmail, boardName)
VALUES ('is@downtonabbey.org', 'Isabel Board 1');

-- Matthew's board
INSERT INTO Boards (loginEmail, boardName)
VALUES ('mat@downtonabbey.org', 'Matthew Board 1');


------------------------------------------------------

-- photo - mary
INSERT INTO Contents (contentID, source, type, url)
VALUES (301, 'mockingJay', 'photo', 'http://elliottingotham.files.wordpress.com/2012/03/ladymary2.jpg');

-- photo - edith
INSERT INTO Contents (contentID, source, type, url)
VALUES (302, 'mockingJay', 'photo', 'http://elliottingotham.files.wordpress.com/2012/03/ladyedith.jpg');

-- photo - sybil
INSERT INTO Contents (contentID, source, type, url)
VALUES (303, 'mockingJay', 'photo', 'http://elliottingotham.files.wordpress.com/2012/03/ladysybil.jpg');

-- photo - cora
INSERT INTO Contents (contentID, source, type, url)
VALUES (304, 'mockingJay', 'photo', 'http://primetime.unrealitytv.co.uk/wp-content/uploads/2012/09/Downton_Abbey_s3_Cora_001_FULL.jpg');

-- photo - robert
INSERT INTO Contents (contentID, source, type, url)
VALUES (305, 'mockingJay', 'photo', 'http://jacksonville.com/sites/default/files/imagecache/superphoto/TV-Downton%20Abbey-Bonn_Mill_0.jpg');

-- photo - violet
INSERT INTO Contents (contentID, source, type, url)
VALUES (306, 'mockingJay', 'photo', 'http://elliottingotham.files.wordpress.com/2012/03/violetatchristmasdinner.jpg');

-- photo - isabel
INSERT INTO Contents (contentID, source, type, url)
VALUES (307, 'mockingJay', 'photo', 'http://www.papermag.com/uploaded_images/penelope-wilton-downton-abbey-s2.jpg');

-- photo - matthew
INSERT INTO Contents (contentID, source, type, url)
VALUES (308, 'mockingJay', 'photo', 'http://www.dan-stevens.co.uk/sites/default/files/images/da_da3.preview.png');


-------------------------------------------------------

-- tag Mary
INSERT INTO Tags (contentID, source, tag)
VALUES (301, 'mockingJay', 'mary');

INSERT INTO Tags (contentID, source, tag)
VALUES (301, 'mockingJay', 'lady');

INSERT INTO Tags (contentID, source, tag)
VALUES (301, 'mockingJay', 'downton');

-- tag Edity
INSERT INTO Tags (contentID, source, tag)
VALUES (302, 'mockingJay', 'edith');

INSERT INTO Tags (contentID, source, tag)
VALUES (302, 'mockingJay', 'downton');

-- tag Sybil
INSERT INTO Tags (contentID, source, tag)
VALUES (303, 'mockingJay', 'sybil');

INSERT INTO Tags (contentID, source, tag)
VALUES (303, 'mockingJay', 'brave');

-- tag Cora
INSERT INTO Tags (contentID, source, tag)
VALUES (304, 'mockingJay', 'cora');

INSERT INTO Tags (contentID, source, tag)
VALUES (304, 'mockingJay', 'mother');

INSERT INTO Tags (contentID, source, tag)
VALUES (304, 'mockingJay', 'america');

-- tag robert
INSERT INTO Tags (contentID, source, tag)
VALUES (305, 'mockingJay', 'robert');

INSERT INTO Tags (contentID, source, tag)
VALUES (305, 'mockingJay', 'father');

-- tag violet
INSERT INTO Tags (contentID, source, tag)
VALUES (306, 'mockingJay', 'violet');

INSERT INTO Tags (contentID, source, tag)
VALUES (306, 'mockingJay', 'granny');

INSERT INTO Tags (contentID, source, tag)
VALUES (306, 'mockingJay', 'hilarious');

-- tag isabel
INSERT INTO Tags (contentID, source, tag)
VALUES (307, 'mockingJay', 'isabel');

INSERT INTO Tags (contentID, source, tag)
VALUES (307, 'mockingJay', 'mother');

-- tag matthew
INSERT INTO Tags (contentID, source, tag)
VALUES (308, 'mockingJay', 'matthew');

INSERT INTO Tags (contentID, source, tag)
VALUES (308, 'mockingJay', 'handsome');

INSERT INTO Tags (contentID, source, tag)
VALUES (308, 'mockingJay', 'lawyer');



------------------------------------------------------

-- pinned by Mary
INSERT INTO Pins (contentID, source, loginEmail, boardName)
VALUES (301, 'mockingJay', 'ma@downtonabbey.org', 'Mary Board 1');

INSERT INTO Pins (contentID, source, loginEmail, boardName)
VALUES (301, 'mockingJay', 'ma@downtonabbey.org', 'Mary Board 2');

INSERT INTO Pins (contentID, source, loginEmail, boardName)
VALUES (308, 'mockingJay', 'ma@downtonabbey.org', 'Mary Board 2');

-- pinneed by Edith
INSERT INTO Pins (contentID, source, loginEmail, boardName)
VALUES (302, 'mockingJay', 'ed@downtonabbey.org', 'Edith Board 1');

-- pinneed by Sybil
INSERT INTO Pins (contentID, source, loginEmail, boardName)
VALUES (303, 'mockingJay', 'sy@downtonabbey.org', 'Sybil Board 1');

INSERT INTO Pins (contentID, source, loginEmail, boardName)
VALUES (301, 'mockingJay', 'sy@downtonabbey.org', 'Sybil Board 1');

INSERT INTO Pins (contentID, source, loginEmail, boardName)
VALUES (302, 'mockingJay', 'sy@downtonabbey.org', 'Sybil Board 1');

-- pinned by Cora
INSERT INTO Pins (contentID, source, loginEmail, boardName)
VALUES (304, 'mockingJay', 'co@downtonabbey.org', 'Cora Board 1');

INSERT INTO Pins (contentID, source, loginEmail, boardName)
VALUES (301, 'mockingJay', 'co@downtonabbey.org', 'Cora Board 1');

INSERT INTO Pins (contentID, source, loginEmail, boardName)
VALUES (302, 'mockingJay', 'co@downtonabbey.org', 'Cora Board 1');

INSERT INTO Pins (contentID, source, loginEmail, boardName)
VALUES (303, 'mockingJay', 'co@downtonabbey.org', 'Cora Board 1');

INSERT INTO Pins (contentID, source, loginEmail, boardName)
VALUES (305, 'mockingJay', 'co@downtonabbey.org', 'Cora Board 1');

-- pinned by robert
INSERT INTO Pins (contentID, source, loginEmail, boardName)
VALUES (305, 'mockingJay', 'ro@downtonabbey.org', 'Robert Board 1');

INSERT INTO Pins (contentID, source, loginEmail, boardName)
VALUES (304, 'mockingJay', 'ro@downtonabbey.org', 'Robert Board 1');

-- pinned by violet

INSERT INTO Pins (contentID, source, loginEmail, boardName)
VALUES (306, 'mockingJay', 'vi@downtonabbey.org', 'Violet Board 1');

INSERT INTO Pins (contentID, source, loginEmail, boardName)
VALUES (305, 'mockingJay', 'vi@downtonabbey.org', 'Violet Board 1');


-- pinned by isabel
INSERT INTO Pins (contentID, source, loginEmail, boardName)
VALUES (307, 'mockingJay', 'is@downtonabbey.org', 'Isabel Board 1');

INSERT INTO Pins (contentID, source, loginEmail, boardName)
VALUES (308, 'mockingJay', 'is@downtonabbey.org', 'Isabel Board 1');

-- pinned by matthew
INSERT INTO Pins (contentID, source, loginEmail, boardName)
VALUES (301, 'mockingJay', 'mat@downtonabbey.org', 'Matthew Board 1');

INSERT INTO Pins (contentID, source, loginEmail, boardName)
VALUES (308, 'mockingJay', 'mat@downtonabbey.org', 'Matthew Board 1');

--------------------------------------------------------

-- mary's rating
INSERT INTO Ratings (contentID, source, loginEmail, rating)
VALUES (301, 'mockingJay', 'ma@downtonabbey.org', 5);

INSERT INTO Ratings (contentID, source, loginEmail, rating)
VALUES (302, 'mockingJay', 'ma@downtonabbey.org', 2);

INSERT INTO Ratings (contentID, source, loginEmail, rating)
VALUES (308, 'mockingJay', 'ma@downtonabbey.org', 5);

-- edith's rating
INSERT INTO Ratings (contentID, source, loginEmail, rating)
VALUES (302, 'mockingJay', 'ed@downtonabbey.org', 5);

-- sybil's rating
INSERT INTO Ratings (contentID, source, loginEmail, rating)
VALUES (307, 'mockingJay', 'sy@downtonabbey.org', 5);

-- cora's rating
INSERT INTO Ratings (contentID, source, loginEmail, rating)
VALUES (305, 'mockingJay', 'co@downtonabbey.org', 5);

-- robert's rating
INSERT INTO Ratings (contentID, source, loginEmail, rating)
VALUES (304, 'mockingJay', 'ro@downtonabbey.org', 5);

-- violet's rating
INSERT INTO Ratings (contentID, source, loginEmail, rating)
VALUES (305, 'mockingJay', 'vi@downtonabbey.org', 5);

INSERT INTO Ratings (contentID, source, loginEmail, rating)
VALUES (304, 'mockingJay', 'vi@downtonabbey.org', 4);

-- isabel's rating
INSERT INTO Ratings (contentID, source, loginEmail, rating)
VALUES (308, 'mockingJay', 'is@downtonabbey.org', 5);

INSERT INTO Ratings (contentID, source, loginEmail, rating)
VALUES (301, 'mockingJay', 'is@downtonabbey.org', 5);

INSERT INTO Ratings (contentID, source, loginEmail, rating)
VALUES (306, 'mockingJay', 'is@downtonabbey.org', 5);

-- matthew's rating
INSERT INTO Ratings (contentID, source, loginEmail, rating)
VALUES (301, 'mockingJay', 'mat@downtonabbey.org', 5);

INSERT INTO Ratings (contentID, source, loginEmail, rating)
VALUES (305, 'mockingJay', 'mat@downtonabbey.org', 5);

INSERT INTO Ratings (contentID, source, loginEmail, rating)
VALUES (307, 'mockingJay', 'mat@downtonabbey.org', 5);

INSERT INTO Ratings (contentID, source, loginEmail, rating)
VALUES (308, 'mockingJay', 'mat@downtonabbey.org', 4);