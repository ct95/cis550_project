--Eric

INSERT INTO Users (loginEmail, firstName, lastName, password, affiliation, interest)
VALUES ('iamthedanger@en.com', 'Walter', 'White', 'password1', 'affiliation', 'chemistry, cooking');

INSERT INTO Users (loginEmail, firstName, lastName, password, affiliation, interest)
VALUES ('swhite@en.com', 'Skyler', 'White', 'password2', 'a1a car wash', 'business, parenting');

INSERT INTO Users (loginEmail, firstName, lastName, password, affiliation, interest)
VALUES ('yo@en.com', 'Jesse', 'Pinkman', 'password3', 'ABQ', 'cooking');

INSERT INTO Users (loginEmail, firstName, lastName, password, affiliation, interest)
VALUES ('bettercallsaul@en.com', 'Saul', 'Goodman', 'password4', 'Saul Goodman Law', 'law');

INSERT INTO Users (loginEmail, firstName, lastName, password, affiliation, interest)
VALUES ('gustavo@en.com', 'Gus', 'Fring', 'password5', 'Los Pollos Hermanos', 'food, business');

INSERT INTO Users (loginEmail, firstName, lastName, password, affiliation, interest)
VALUES ('hanks@en.com', 'Hank', 'Schrader', 'password6', 'DEA', 'minerals');

INSERT INTO Users (loginEmail, firstName, lastName, password, affiliation, interest)
VALUES ('maries@en.com', 'Marie', 'Schrader', 'password7', 'DEA', 'purple, shopping');

INSERT INTO Users (loginEmail, firstName, lastName, password, affiliation, interest)
VALUES ('ehrmantraut@en.com', 'Mike', 'Ehrmantraut', 'password8', 'Philadelphia police', 'philadelphia, cleaning');

------------------------------------------------------------------------------------

INSERT INTO Friends (loginEmail, friendLogin)
VALUES ('iamthedanger@en.com', 'swhite@en.com');

INSERT INTO Friends (loginEmail, friendLogin)
VALUES ('iamthedanger@en.com', 'yo@en.com');

INSERT INTO Friends (loginEmail, friendLogin)
VALUES ('swhite@en.com', 'maries@en.com');

INSERT INTO Friends (loginEmail, friendLogin)
VALUES ('swhite@en.com', 'hanks@en.com');

INSERT INTO Friends (loginEmail, friendLogin)
VALUES ('hanks@en.com', 'maries@en.com');

INSERT INTO Friends (loginEmail, friendLogin)
VALUES ('maries@en.com', 'hanks@en.com');

INSERT INTO Friends (loginEmail, friendLogin)
VALUES ('gustavo@en.com', 'ehrmantraut@en.com');

INSERT INTO Friends (loginEmail, friendLogin)
VALUES ('gustavo@en.com', 'yo@en.com');

INSERT INTO Friends (loginEmail, friendLogin)
VALUES ('yo@en.com', 'ehrmantraut@en.com');

INSERT INTO Friends (loginEmail, friendLogin)
VALUES ('yo@en.com', 'gustavo@en.com');

INSERT INTO Friends (loginEmail, friendLogin)
VALUES ('yo@en.com', 'iamthedanger@en.com');

------------------------------------------------------------------------------------

INSERT INTO Boards (loginEmail, boardName)
VALUES ('iamthedanger@en.com', 'Knock Knock');

INSERT INTO Boards (loginEmail, boardName)
VALUES ('swhite@en.com', 'SkyBoard');

INSERT INTO Boards (loginEmail, boardName)
VALUES ('yo@en.com', 'Stacks');

INSERT INTO Boards (loginEmail, boardName)
VALUES ('bettercallsaul@en.com', 'SaulsBoard');

INSERT INTO Boards (loginEmail, boardName)
VALUES ('iamthedanger@en.com', 'Chemistry Board');

INSERT INTO Boards (loginEmail, boardName)
VALUES ('gustavo@en.com', 'GustavosBoard');

------------------------------------------------------------------------------------

INSERT INTO Contents (contentID, source, type, url)
VALUES (100, 'mockingJay', 'photo', 'http://i.imgur.com/Cxagv.jpg');

INSERT INTO Contents (contentID, source, type, url)
VALUES (101, 'mockingJay', 'photo', 'http://i.imgur.com/DR4rpJZ.jpg');

INSERT INTO Contents (contentID, source, type, url)
VALUES (102, 'mockingJay', 'photo', 'http://i.imgur.com/19p5W.jpg');

INSERT INTO Contents (contentID, source, type, url)
VALUES (103, 'mockingJay', 'photo', 'https://pbs.twimg.com/media/BK86vN0CEAEEb0o.jpg');

INSERT INTO Contents (contentID, source, type, url)
VALUES (104, 'mockingJay', 'photo', 'http://upload.wikimedia.org/wikipedia/commons/b/b5/Benjamin_Franklin_statue_in_front_of_College_Hall.JPG');

INSERT INTO Contents (contentID, source, type, url)
VALUES (105, 'mockingJay', 'photo', 'http://m.cvs.com/img/c/gw_320/ttl_30d/quality_100/1url_akimages.shoplocal.com/dyn_li/200.0.88.0/Retailers/CVS/131006_XX_04_1__1.JPG');

INSERT INTO Contents (contentID, source, type, url)
VALUES (106, 'mockingJay', 'photo', 'http://i.imgur.com/S6ygiKx.jpg');

------------------------------------------------------------------------------------

INSERT INTO Tags (contentID, source, tag)
VALUES (105, 'mockingJay', 'penn');

INSERT INTO Tags (contentID, source, tag)
VALUES (103, 'mockingJay', 'clothes');

INSERT INTO Tags (contentID, source, tag)
VALUES (106, 'mockingJay', 'kong');

INSERT INTO Tags (contentID, source, tag)
VALUES (102, 'mockingJay', 'murray');

INSERT INTO Tags (contentID, source, tag)
VALUES (105, 'mockingJay', 'water');

------------------------------------------------------------------------------------

INSERT INTO Pins (contentID, source, loginEmail, boardName)
VALUES (105, 'mockingJay', 'iamthedanger@en.com', 'Knock Knock');

INSERT INTO Pins (contentID, source, loginEmail, boardName)
VALUES (102, 'mockingJay', 'iamthedanger@en.com', 'Knock Knock');

INSERT INTO Pins (contentID, source, loginEmail, boardName)
VALUES (106, 'mockingJay', 'yo@en.com', 'Stacks');

INSERT INTO Pins (contentID, source, loginEmail, boardName)
VALUES (105, 'mockingJay', 'swhite@en.com', 'SkyBoard');

------------------------------------------------------------------------------------

INSERT INTO Ratings (contentID, source, loginEmail, rating)
VALUES (102, 'mockingJay', 'iamthedanger@en.com', 5);

INSERT INTO Ratings (contentID, source, loginEmail, rating)
VALUES (101, 'mockingJay', 'iamthedanger@en.com', 5);

INSERT INTO Ratings (contentID, source, loginEmail, rating)
VALUES (105, 'mockingJay', 'maries@en.com', 4);

INSERT INTO Ratings (contentID, source, loginEmail, rating)
VALUES (105, 'mockingJay', 'bettercallsaul@en.com', 5);